# Tor overlay network bootstrapping

## Usage

```
usage: src/rtun.py [-h] [-r RELAY] [-g GUARD] [-l] [-c] [-k COOKIE] [-p] [-K] [-t TUNNEL_NAME] [-n NAMESPACE] [-i ID] [-d DID] [-R] [-x]

optional arguments:
  -h, --help            show this help message and exit
  -r RELAY, --relay RELAY
                        relay to be used as rendezvous point
  -g GUARD, --guard GUARD
                        optional router to be used as guard node
  -l, --listen          create the rendezvous point an wait
  -c, --connect         connect to an already established rendezvous point
  -k COOKIE, --cookie COOKIE
                        a 20 byte rendezvous cookie
  -p, --pairwise        select relay automatically using the pairwise algorithm
  -t TUNNEL_NAME, --tunnel_name TUNNEL_NAME
                        name of the tunnel, default is the two peers name concatenated
  -n NAMESPACE, --namespace NAMESPACE
                        secret key used to differentiate between different nets
  -i ID, --id ID        id of our own client(used for addressing and port allocation)
  -d DID, --did DID     destination id
  -R, --renew           renew Tor directory server list
  -x, --dummy           do not connect, only test
```

## Installation guide

### Prerequisites

- openVPN
- Python3
- Pip3
- git

### Virtualenv

Create a virtual environment for Python:

```bash
pip3 install virtualenv
python3 -m virtualenv -p python3 venv
source venv/bin/activate
```

This creates a new folder `venv/`, with a virtual python environment and activates it. This makes it so you can keep the required packages seperate from other projects. Write `deactivate` to exit the virtual environment.

### Installing dependencies

While inside of the virtual environment.

```bash
pip install -r requirements.txt -r requirements-dev.txt
pip install -e . # install the src file module
```

This install all the dependencies and puts them in the `venv/` folder. It also fixes imports of local packages.

**NB** We use our own fork of the package `torpy`. This should be downloaded automaticly with the commands above.

### Populating tor consensus relay list

Torpy stores the list of available Tor relays in the file `/root/.local/share/torpy/network_status`. To populate the for the first time, or renewit it, you can run the script `scripts/renew_consensus.bash`. The consensus file should be updated autmaticly with the onion bootstrapper.

The consensus must be the same for all peers to be able to communicate.

## Setting up a connection between two peers

You can test that everyting works with trying to setup a connection betwwen two peers.
Open up two terminal windows and for bot run the following:

```bash
sudo su //needs root priviliges
. venv/bin/activate
```

Then you will have to choose one of them as the server, and the other as client.

For the server run:

```bash
python src/rtun.py --listen --keys --relay-chooser keys --id 1 --did 2
```

This prints the private/public keys and prompts for the other peers public key.

For the client run:

```bash
python src/rtun.py --connect --keys --relay-chooser keys --id 2 --did 1
```

input the respective public keys to both of them

If everything works both of them should say connected.

The connection sets up an openVPN tunnel. The connection can then be testet with `ping 10.<did>.0.1`

## Tests

Tests can be run with pytest.

```bash
. venv/bin/activate

python -m pytest // or just pytest
```

**NB** Some tests may need root privilages

## IPFS

The IPFS module requires go-IPFS to be installed.

https://docs.ipfs.eth.link/install/command-line/#linux

Or the following (might be outdated)

```bash
wget https://dist.ipfs.tech/kubo/v0.19.0/kubo_v0.19.0_linux-amd64.tar.gz
tar -xvzf kubo_v0.19.0_linux-amd64.tar.gz
cd kubo
sudo bash install.sh
```

**NB** We use IPFS pubsub witch it set to be deprecated and may not work in the future.

The IPFS daemon must be started with the --enable-experimantal-pubsub to be able to send pubsub messages. `ipfs daemon --enable-pubsub-experiment`

## Onion bootstrapper

The onion bootstrapper needs tor to be installed. You also need to edit the tor configuration file (`torrc`) to open the Tor Control server used by the stem package for programticly creating hidden services.
The needed configuration can be found in this repositories `torrc` file. Swap it out with `/etc/tor/torrc`.

start the tor service with `service tor start`. If there are error when setting up the onion server try to restart the tor service with `service tor restart`. Group onion flask server runs at port 5006. Private onion flask server runs at port 5000.

The onion bootstrapper can be runned with the follwing command:

```
python src/bootstrapping/onion/bootstrapper.py -s "shared_secret" -d 0
```

It will check if the group_onion server is up and create it if it is not. The Group onion server is working like a DHCP server for giving out internal ips (we call it id) in the network. If and when the group onion server is up. It will try to register at the group_server. When registered the private onion server starts and waits for connections. A connector also starts, witch will periodicly fetch the list of registerd users and try to connect to some of them if it wants more connections.
