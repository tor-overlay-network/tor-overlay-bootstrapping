if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

echo "This script needs torpy to be installed to renew the consensus"

# removes the existing file if it exists
rm /root/.local/share/torpy/network_status 2> /dev/null

. venv/bin/activate
python3 -c "import src.bootstrapping.utils as utils; utils.update_torpy_consensus_if_necessary()"


