from __future__ import annotations
import json
import os
import hashlib
import time

from cryptography.hazmat.backends.openssl.backend import Backend
from cryptography.hazmat.primitives import serialization
from cryptography.exceptions import InvalidSignature

from nacl.signing import SigningKey

import src.bootstrapping.utils as utils


def get_shared_secret(my_private: bytes, peer_public: bytes, type="x25519"):
    if not type == "x25519":
        raise NotImplementedError("Only x25519 is implemented")
    backend = Backend()
    private_key = backend.x25519_load_private_bytes(my_private)
    public_key = backend.x25519_load_public_bytes(peer_public)
    shared_secret = private_key.exchange(public_key)
    if all(b == 0 for b in shared_secret):
        raise ValueError("Shared secret is all zeros")
    return shared_secret


def get_public_key(private_key: bytes, type="x25519"):
    backend = Backend()
    if type == "x25519":
        private = backend.x25519_load_private_bytes(private_key)
        return private.public_key().public_bytes(
            serialization.Encoding.Raw, serialization.PublicFormat.Raw
        )
    if type == "ed25519":
        private = backend.ed25519_load_private_bytes(private_key)
        return private.public_key().public_bytes(
            serialization.Encoding.Raw, serialization.PublicFormat.Raw
        )
    raise NotImplementedError("Only x25519 and ed25519 is implemented")


def get_public_key2(private_key: bytes, type="ed25519"):
    if type == "ed25519":
        private_key = SigningKey(seed=private_key)
        public_key = private_key.verify_key
        return public_key.encode()

    raise NotImplementedError("Only ed25519 is implemented")


def verify_signature(public_key: bytes, signature: bytes, data: bytes) -> bool:
    backend = Backend()
    public = backend.ed25519_load_public_bytes(public_key)
    try:
        public.verify(signature, data)
        return True
    except InvalidSignature:
        return False


def get_hash_from_data(data: dict) -> bytes:
    stringified_data = json.dumps(data, sort_keys=True, default=str)
    return hashlib.sha256(stringified_data.encode("utf-8")).digest()


def add_signature_to_data(signing_key: bytes, verify_key: bytes, data: dict) -> dict:
    h = get_hash_from_data(data)
    signed_h = sign_data(signing_key, h)
    is_verified = verify_signature(verify_key, signed_h, h)
    if not is_verified:
        raise Exception("sign is not valid")
    data["sign"] = signed_h.hex()
    return data


def sign_data(signing_key: bytes, data: bytes) -> bytes:
    backend = Backend()
    private = backend.ed25519_load_private_bytes(signing_key)
    return private.sign(data)


def generate_new_raw_key_pair(type="x25519"):
    backend = Backend()
    if type == "x25519":
        private_key = backend.x25519_generate_key()
        public_key = private_key.public_key()
        return (
            private_key.private_bytes(
                encoding=serialization.Encoding.Raw,
                format=serialization.PrivateFormat.Raw,
                encryption_algorithm=serialization.NoEncryption(),
            ),
            public_key.public_bytes(
                serialization.Encoding.Raw, serialization.PublicFormat.Raw
            ),
        )
    if type == "ed25519":
        private_key = backend.ed25519_generate_key()
        public_key = private_key.public_key()
        return (
            private_key.private_bytes(
                encoding=serialization.Encoding.Raw,
                format=serialization.PrivateFormat.Raw,
                encryption_algorithm=serialization.NoEncryption(),
            ),
            public_key.public_bytes(
                serialization.Encoding.Raw, serialization.PublicFormat.Raw
            ),
        )

    raise NotImplementedError("Only x25519 and ed25519 is implemented")


def generate_new_raw_key_pair2(type="ed25519"):
    if type == "ed25519":
        signing_key = SigningKey.generate()
        verify_key = signing_key.verify_key
        return (
            signing_key.encode(),  # seed
            signing_key._signing_key,  # private key
            verify_key.encode(),  # public key
        )

    raise NotImplementedError("Only ed25519 is implemented")


def derive_new_key_from_secret(secret: bytes) -> bytes:
    """Briar spec uses: KDF(k, x_1, ..., x_n) = MAC(k, int_32(len(x_1)) || x_1 || ... || int_32(len(x_n)) || x_n) where mac is blake2b"""
    new_key = hashlib.pbkdf2_hmac(
        "sha256", secret, salt=secret[0:20], iterations=100000, dklen=256
    )
    return new_key


def get_derived_secret_from_keys(private_key: bytes, public_key: bytes) -> bytes:
    """Derive a secret from a private and a public key"""
    shared_secret_from_keys = get_shared_secret(private_key, public_key)
    return derive_new_key_from_secret(shared_secret_from_keys)


def store_key_to_file(private_key: bytes, filename: str = "_private.key"):
    if not os.path.exists("keys"):
        os.makedirs("keys")
    utils.store_bytes_to_file(private_key, f"keys/{filename}")
    return filename


def read_key_from_file(filename: str = "_private.key"):
    return utils.read_bytes_from_file(f"keys/{filename}")


def get_or_create_private_key_file(id: int = 0, type="x25519"):
    print("get_or_create_private_key_file", id, type)
    try:
        time.sleep(2)
        private_key = read_key_from_file(f"_{type}-private{id}.key")
    except FileNotFoundError:
        private_key, _ = generate_new_raw_key_pair(type)
        store_key_to_file(private_key, f"_{type}-private{id}.key")
    return private_key


def get_or_create_key_pair_file(id: int = 0, type="x25519"):
    print("get_or_create_key_pair_file", id, type)
    try:
        private_key = read_key_from_file(f"_{type}-private{id}.key")
    except FileNotFoundError:
        private_key, public_key1 = generate_new_raw_key_pair(type)
        store_key_to_file(private_key, f"_{type}-private{id}.key")
        public_key2 = get_public_key(private_key, type)
        assert public_key1 == public_key2
    return private_key, get_public_key(private_key, type)


def get_or_create_key_pair_file2(id: int = 0, type="x25519"):
    try:
        private_key = read_key_from_file(f"_{type}-private{id}.key")
    except FileNotFoundError:
        private_key, _ = generate_new_raw_key_pair2(type)
        store_key_to_file(private_key, f"_{type}-private{id}.key")
    public_key = get_public_key2(private_key, type)
    return private_key, public_key


class ED25519KeyHandler:
    def __init__(self, signing_key: bytes = None):
        print("init ed25519handler")
        self._signing_key = None
        self._verify_key = None
        if signing_key != None:
            self.set_keys_from_signing_key(signing_key)

    @property
    def signing_key_bytes(self) -> bytes:
        if self._signing_key == None:
            raise ValueError("Signing key is not set")
        return self._signing_key

    @property
    def signing_key_hex(self) -> hex:
        if self._signing_key == None:
            raise ValueError("Signing key is not set")
        return self._signing_key.hex()

    @property
    def verify_key_bytes(self) -> bytes:
        if self._verify_key == None:
            raise ValueError("Verify key is not set")
        return self._verify_key

    @property
    def verify_key_hex(self) -> hex:
        if self._verify_key == None:
            raise ValueError("Verify key is not set")
        return self._verify_key.hex()

    def sign(self, message: bytes) -> bytes:
        if self._signing_key == None:
            raise ValueError("Signing is not set")
        return sign_data(self._signing_key, message)

    def verify_message(self, signature: bytes, message: bytes) -> bytes:
        if self._verify_key == None:
            raise ValueError("Verify key is not set")
        return verify_signature(self._verify_key, signature, message)

    def _check_if_signing_key_is_valid(self, signing_key: bytes) -> None:
        if not isinstance(signing_key, bytes):
            raise ValueError("Signing key must be bytes")
        if len(signing_key) != 32:
            raise ValueError("Signing key must be 32 bytes")

    def _check_if_keys_are_already_set(self) -> None:
        if self._signing_key is not None and self._verify_key is not None:
            raise ValueError("Signing and verify is already set")

    def clear_keys(self) -> None:
        self._signing_key = None
        self._verify_key = None

    def set_keys(self, signing_key: bytes, verify_key: bytes) -> ED25519KeyHandler:
        self._check_if_keys_are_already_set()
        if not isinstance(signing_key, bytes) and not isinstance(verify_key, bytes):
            raise ValueError("Signing and verify key must be bytes")
        if get_public_key(signing_key, "ed25519") != verify_key:
            raise ValueError("Signing and verify key do not match")
        self._signing_key = signing_key
        self._verify_key = verify_key
        return self

    def set_keys_from_signing_key(self, signing_key: bytes) -> ED25519KeyHandler:
        self._check_if_keys_are_already_set()
        self._check_if_signing_key_is_valid(signing_key)
        self._signing_key = signing_key
        self._verify_key = get_public_key(signing_key, "ed25519")
        return self

    def set_keys_from_secret(self, secret: bytes) -> ED25519KeyHandler:
        self._check_if_keys_are_already_set()
        h = hashlib.sha256(secret).digest()
        print(f"hash from secret, {h.hex()}", secret)
        self._signing_key = h
        self._verify_key = get_public_key(h, "ed25519")
        return self

    def generate_new_keys(self) -> ED25519KeyHandler:
        self._check_if_keys_are_already_set()
        self._signing_key, self._verify_key = generate_new_raw_key_pair("ed25519")
        return self


if __name__ == "__main__":
    # v3 spec https://gitweb.torproject.org/torspec.git/plain/rend-spec-v3.txt
    # TODO: https://github.com/onionshare/onionshare/issues/461
    private, public = get_or_create_private_key_file(1, "ed25519")
    print(f"ed25519 private: \t{private.hex()}\t length: {len(private)} bytes")
    print(f"ed25519 public:\t{public.hex()}\t length: {len(public)} bytes")
    file_content1 = read_key_from_file("hs_ed25519_secret_key")
    first_bytes = file_content1[0:32]
    last_bytes = file_content1[-64:]
    print(f"file_content1: {file_content1}, length: {len(file_content1)} bytes")
    print(f"first_bytes: {first_bytes}, length: {len(first_bytes)} bytes")
    print(f"last_bytes: {last_bytes.hex()}, length: {len(last_bytes.hex())} bytes")
