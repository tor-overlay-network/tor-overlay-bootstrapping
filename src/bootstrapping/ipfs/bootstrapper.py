import argparse
import datetime
from enum import Enum
import heapq
import logging
import os
import time
from stmpy import Driver

import src.bootstrapping.state_machine as state_machine
import src.bootstrapping.ipfs.pubusb as pubusb
import src.bootstrapping.crypto as crypto
import src.bootstrapping.ipfs.utils as ipfs_utils
import src.bootstrapping.utils as utils
import src.routing.utils as connection_utils

import src.routing.connection as con

logging.getLogger("stmpy").setLevel(logging.CRITICAL)
logging.getLogger()


GLOBAL_TOPIC = "bootstrapping"

PUBLISH_GLOBAL_EVERY = 7


class MachineNames(Enum):
    GlobalAdvertiser = "global_advertiser"
    GlobalListener = "global_listener"
    PrivateListener = "private_listener"
    PeerChannel = "peer_channel"
    Connecter = "connecter"
    Bootstrapper = "bootstrapper"


class GlobalAdvertiser(state_machine.MachineBase):
    def __init__(
        self, bootstrapper, log_state_entries=state_machine.DEFAULT_LOG_STATE_ENTRIES
    ):
        self.name = MachineNames.GlobalAdvertiser.value
        self.publisher = pubusb.IPFSPublisher()
        self.states = [
            {
                "name": "idle",
            },
            {
                "name": "timer",
                "entry": f"publish_global(); start_timer('t', {PUBLISH_GLOBAL_EVERY* 1000});",
                "exit": "stop_timer('t');",
            },
        ]
        self.transitions = [
            {
                "source": "initial",
                "target": "idle",
            },
            {
                "trigger": "start",
                "source": "idle",
                "target": "timer",
            },
            {
                "trigger": "stop",
                "source": "timer",
                "target": "idle",
            },
            {
                "trigger": "t",
                "source": "timer",
                "target": "timer",
            },
        ]
        super().__init__(bootstrapper, log_state_entries=log_state_entries)

    def publish_global(self):
        self._logger.info(f"publish_global")
        packet = ipfs_utils.create_data_packet(
            self.bootstrapper.get_signing_key(),
            self.bootstrapper.get_verify_key(),
            sender=self.bootstrapper.get_verify_key().hex(),
            nonce=os.urandom(32),
        )
        self.publisher.publish(
            f"{GLOBAL_TOPIC}",
            packet,
        )


class Connecter(state_machine.MachineBase):
    def __init__(
        self, bootstrapper, log_state_entries=state_machine.DEFAULT_LOG_STATE_ENTRIES
    ):
        self.name = MachineNames.Connecter.value
        self.states = [
            {
                "name": "idle",
            },
            {
                "name": "timer",
                "entry": "check_if_want_connections(); start_timer('t', 5000)",
                "exit": "stop_timer('t');",
            },
            {
                "name": "start_peer_machine",
                "entry": "start_peer_stm()",
            },
        ]
        self.transitions = [
            {
                "source": "initial",
                "target": "idle",
            },
            {
                "trigger": "start",
                "source": "idle",
                "target": "timer",
            },
            {
                "trigger": "stop",
                "source": "timer",
                "target": "idle",
            },
            {
                "trigger": "t",
                "source": "timer",
                "target": "timer",
            },
            {
                "trigger": "want_connections",
                "source": "timer",
                "target": "start_peer_machine",
            },
            {
                "trigger": "empty_queue",
                "source": "start_peer_machine",
                "target": "timer",
            },
            {
                "trigger": "peer_stm_started",
                "source": "start_peer_machine",
                "target": "timer",
            },
        ]
        super().__init__(bootstrapper, log_state_entries=log_state_entries)

    def check_if_want_connections(self):
        self._logger.info(f"check_if_want_connections")
        if self.bootstrapper.get_want_connections():
            self.machine.send("want_connections")

    def start_peer_stm(self):
        self._logger.info(f"start_peer_stm")
        peer_public = self.bootstrapper.get_first_public_key()
        if peer_public is None:
            self.machine.send("empty_queue")
        connectionHandler: con.ConnectionHandler = self.bootstrapper.connectionHandler
        if connectionHandler.is_peer_public_connected(peer_public.hex()):
            self._logger.info(f"Already connected to peer")
            self.machine.send("empty_queue")
        else:
            self.bootstrapper.create_peer_channel_machine(peer_public)
            self.machine.send("peer_stm_started")


class GlobalListener(state_machine.MachineBase):
    def __init__(
        self, bootstrapper, log_state_entries=state_machine.DEFAULT_LOG_STATE_ENTRIES
    ):
        self.name = MachineNames.GlobalListener.value
        self.subscription = None
        self.last_verified_peer_id = None
        self.states = [
            {
                "name": "idle",
                "entry": f"send_to('start', '{MachineNames.Connecter.value}'); unsubscribe_to_global()",
                "exit": f"send_to('stop', '{MachineNames.Connecter.value}'); subscribe_to_global()",
            },
            {
                "name": "listening",
                "exit": "stop_timer('no_messages');",
            },
            {
                "name": "start_peer_conversation",
                "entry": "start_peer_stm()",
                "message_received": "defer",
            },
        ]

        self.transitions = [
            {
                "source": "initial",
                "target": "idle",
            },
            {
                "trigger": "start",
                "source": "idle",
                "target": "listening",
                "effect": f'start_timer("no_messages", {(3* PUBLISH_GLOBAL_EVERY  * 1000) if bootstrapper.global_listener_wait_for is None else bootstrapper.global_listener_wait_for })',
            },
            {
                "trigger": "stop",
                "source": "listening",
                "target": "idle",
            },
            {
                "trigger": "no_messages",
                "source": "listening",
                "target": "listening",
                "effect": f"send_to('no_global_activity', '{MachineNames.Bootstrapper.value}')",
            },
            {
                "trigger": "good_message",
                "source": "listening",
                "target": "start_peer_conversation",
            },
            {
                "trigger": "public_key_added",
                "source": "start_peer_conversation",
                "target": "listening",
            },
        ]
        super().__init__(bootstrapper, log_state_entries=log_state_entries)

    def subscribe_to_global(self):
        if self.subscription:
            self._logger.error(f"subscription already exists")
            return
        self.subscription = pubusb.IPFSSubscriptionStream(
            f"{GLOBAL_TOPIC}", self.on_global_message
        )
        self.subscription.start()
        self._logger.info(f"subscribed to global")

    def unsubscribe_to_global(self):
        if not self.subscription:
            self._logger.error(f"subscription does not exist")
            return
        self.subscription.stop()

    def on_global_message(self, *_, **kwargs):
        if not "data" in kwargs:
            return
        try:
            verfied_data = ipfs_utils.get_verfied_data_from_packet(kwargs["data"])
        except Exception as e:
            self._logger.error(f"bad data: {e}")
            return
        try:
            public_key_bytes = bytes.fromhex(verfied_data["sender"])
            self.last_verified_peer_id = public_key_bytes
            if public_key_bytes == self.bootstrapper.get_verify_key():
                return
            if self.bootstrapper.connectionHandler.is_peer_public_connected(
                public_key_bytes.hex()
            ):
                return
            self.bootstrapper.add_public_key(public_key_bytes)
        except Exception as e:
            self._logger.error(f"bad public key {e}")
            return
        self.machine.send("good_message")

    def start_peer_stm(self):
        self._logger.info(f"start_peer_stm")
        self.bootstrapper.create_peer_channel_machine(self.last_verified_peer_id)
        self.machine.send("public_key_added")


class PrivateListener(state_machine.MachineBase):
    def __init__(
        self, bootstrapper, log_state_entries=state_machine.DEFAULT_LOG_STATE_ENTRIES
    ):
        self.name = MachineNames.PrivateListener.value
        self.my_DH_keys = None
        self.subscription = None
        self.publisher = pubusb.IPFSPublisher()
        self.messageQueue = PrioritySet()
        self.peer_id = None
        self.private_topic = None

        self.states = [
            {
                "name": "idle",
            },
            {
                "name": "listening",
            },
            {
                "name": "open_connection",
                "entry": "on_open_connection()",
                "good_message": "defer",
            },
        ]

        self.transitions = [
            {
                "source": "initial",
                "target": "idle",
            },
            {
                "trigger": "start",
                "source": "idle",
                "target": "listening",
                "effect": "subscribe_to_private()",
            },
            {
                "trigger": "stop",
                "source": "listening",
                "target": "idle",
                "effect": "unsubscribe_to_private()",
            },
            {
                "trigger": "good_message",
                "source": "listening",
                "target": "open_connection",
            },
            {
                "trigger": "connections_full",
                "source": "open_connection",
                "target": "listening",
                "effect": "publish_to_peer(*)",
            },
            {
                "trigger": "connection_started",
                "source": "open_connection",
                "target": "listening",
                "effect": "publish_to_peer(*)",
            },
        ]

        super().__init__(bootstrapper, log_state_entries=log_state_entries)

    def subscribe_to_private(self):
        if self.subscription:
            self._logger.error(f"subscription already exists")
            return
        self.private_topic = (
            f"{GLOBAL_TOPIC}/{self.bootstrapper.get_verify_key().hex()}"
        )
        self.subscription = pubusb.IPFSSubscriptionStream(
            self.private_topic, self.on_private_message
        )
        self.subscription.start()
        print(f"subscribed to private", self.private_topic)

    def unsubscribe_to_private(self):
        if not self.subscription:
            self._logger.error(f"subscription does not exist")
            return
        self.subscription.stop()
        self._logger.info(f"unsubscribed from private")

    def on_private_message(self, *_, **kwargs):
        self._logger.info(f"on_private_message")
        if not "data" in kwargs:
            return
        try:
            verfied_data = ipfs_utils.get_verfied_data_from_packet(kwargs["data"])
        except Exception as e:
            self._logger.error(f"bad data: {e}")
            return
        try:
            peer_id = bytes.fromhex(verfied_data["sender"])
            if peer_id == self.bootstrapper.get_verify_key():
                self._logger.debug(f"got my own message - on_private_message")
                return
        except Exception as e:
            self._logger.error(f"bad public key {e}")
            return
        connectionHandler: con.ConnectionHandler = self.bootstrapper.connectionHandler
        if connectionHandler.is_peer_public_connected(peer_id.hex()):
            self._logger.debug(f"already connected to peer")
            return
        try:
            peer_DH = bytes.fromhex(verfied_data["DH"])
            self.messageQueue.add((peer_id, peer_DH), 0)
        except Exception as e:
            self._logger.error(f"bad DH: {e}")
            return
        self._logger.info(f"good_message sending")
        self.machine.send("good_message")

    def on_open_connection(self):
        self._logger.info(f"on_open_connection")
        peer_id, peer_DH = self.messageQueue.pop()
        my_DH_private, my_DH_public = crypto.generate_new_raw_key_pair(type="x25519")
        secret = crypto.get_derived_secret_from_keys(my_DH_private, peer_DH)
        self._logger.info(f"my_id:\t {self.bootstrapper.get_verify_key().hex()}")
        self._logger.info(f"peer_id:\t {peer_id.hex()}")
        self._logger.info(f"secret:\t {secret.hex()}")
        connectionHandler: con.ConnectionHandler = self.bootstrapper.connectionHandler
        if not connectionHandler.can_add_new_connection:
            self._logger.error(f"connectionHandler is full")
            self.machine.send(
                "connections_full",
                kwargs={"max_connections": True, "peer_id": peer_id.hex()},
            )
            return
        connection_utils.create_connection_threaded(
            connectionHandler=connectionHandler,
            my_private_DH=my_DH_private.hex(),
            peer_public_DH=peer_DH.hex(),
            peer_id=ipfs_utils.public_key_to_id(peer_id),
            peer_verify_key=peer_id.hex(),
            is_server=False,
        )

        self.machine.send(
            "connection_started",
            kwargs={"my_DH": my_DH_public, "peer_id": peer_id.hex()},
        )

    def publish_to_peer(self, *_, **kwargs):
        print(f"publish_to_peer")
        if not "peer_id" in kwargs:
            print(f"peer_id missing")
            return
        if "max_connections" in kwargs and kwargs["max_connections"] is True:
            print(f"max_connections")
            packet = ipfs_utils.create_data_packet(
                self.bootstrapper.get_signing_key(),
                self.bootstrapper.get_verify_key(),
                to=kwargs["peer_id"],
                sender=self.bootstrapper.get_verify_key().hex(),
                max_connections=True,
            )
        else:
            if not "my_DH" in kwargs:
                print(f"my_DH is missing")
                return
            packet = ipfs_utils.create_data_packet(
                self.bootstrapper.get_signing_key(),
                self.bootstrapper.get_verify_key(),
                to=kwargs["peer_id"],
                sender=self.bootstrapper.get_verify_key().hex(),
                DH=kwargs["my_DH"],
                nonce=os.urandom(32),
            )
        self.publisher.publish(
            self.private_topic,
            packet,
        )


class PeerChannel(state_machine.MachineBase):
    def __init__(
        self,
        bootstrapper,
        peer_id: str,
        publish_interval=5000,
        log_state_entries=state_machine.DEFAULT_LOG_STATE_ENTRIES,
    ):
        self.name = f"{MachineNames.PeerChannel.value}_{peer_id}"
        self.shared_secret = None

        self.my_DH_keys = None
        self.subscription = None
        self.publisher = pubusb.IPFSPublisher()
        self.peer_id = peer_id
        self.peer_DH_public = None
        self.states = [
            {
                "name": "initialization",
                "entry": "generate_DH_keys()",
                "exit": "subscribe_to_peer()",
            },
            {"name": "publish_to_peer", "entry": "publish_to_peer()"},
            {
                "name": "listen_messages",
                "entry": f"start_timer('publish_again', {publish_interval})",
                "exit": "stop_timer('publish_again')",
            },
            {
                "name": "open_connection",
                "entry": "stop_timer('timeout'); on_open_connection()",
            },
            {
                "name": "off",
                "entry": "unsubscribe_from_peer(); end_stm()",
            },
        ]

        self.transitions = [
            {
                "source": "initial",
                "target": "initialization",
            },
            {
                "trigger": "subscribe",
                "source": "initialization",
                "target": "publish_to_peer",
                "effect": "start_timer('timeout', 15000)",
            },
            {
                "trigger": "message_published",
                "source": "publish_to_peer",
                "target": "listen_messages",
            },
            {
                "trigger": "publish_again",
                "source": "listen_messages",
                "target": "publish_to_peer",
            },
            {
                "trigger": "timeout",
                "source": "listen_messages",
                "target": "off",
            },
            {
                "trigger": "timeout",
                "source": "publish_to_peer",
                "target": "off",
            },
            {
                "trigger": "on_peer_response",
                "source": "publish_to_peer",
                "target": "open_connection",
            },
            {
                "trigger": "on_peer_response",
                "source": "listen_messages",
                "target": "open_connection",
            },
            {
                "trigger": "too_many_messages",
                "source": "publish_to_peer",
                "target": "off",
            },
            {
                "trigger": "too_many_connections",
                "source": "publish_to_peer",
                "target": "off",
            },
            {
                "trigger": "connection_started",
                "source": "open_connection",
                "target": "off",
                "effect": f"send_to('connected', '{MachineNames.Bootstrapper.value}')",
            },
        ]

        super().__init__(bootstrapper, log_state_entries=log_state_entries)

    def subscribe_to_peer(self):
        self._logger.info(f"subscribe_to_peer")
        if self.subscription:
            self._logger.error(f"subscription already exists")
            return
        self.subscription = pubusb.IPFSSubscriptionStream(
            f"{GLOBAL_TOPIC}/{self.peer_id}", self.on_peer_channel_message
        )
        self.subscription.start()
        self._logger.info(f"subscribed to peer channel {self.peer_id}")

    def generate_DH_keys(self):
        self._logger.info(f"generate_DH_keys")
        self.my_DH_keys = crypto.generate_new_raw_key_pair("x25519")
        self.machine.send("subscribe")

    def unsubscribe_from_peer(self):
        if not self.subscription:
            self._logger.error(f"subscription does not exist")
            return
        self.subscription.stop()
        self._logger.info(f"unsubscribed from peer channel {self.peer_id}")

    def on_peer_channel_message(self, *args, **kwargs):
        if not "data" in kwargs:
            return
        data = kwargs["data"]
        try:
            verfied_data = ipfs_utils.get_verfied_data_from_packet(data)
        except Exception as e:
            self._logger.error(f"bad data: {e}")
            return
        try:
            id = bytes.fromhex(verfied_data["sender"])
            if id == self.bootstrapper.get_verify_key():
                self._logger.debug(f"got my own message - on_peer_channel_message")
                return
        except Exception as e:
            self._logger.error(f"bad public key {e}")
            return

        if "max_connections" in verfied_data:
            self._logger.info(f"got max_connections")
            self.machine.send("too_many_connections")
            return

        try:
            peer_DH_public = bytes.fromhex(verfied_data["DH"])
            self.peer_DH_public = peer_DH_public
        except Exception as e:
            self._logger.error(f"bad DH: {e}")
            return
        self.machine.send("on_peer_response")

    def publish_to_peer(self):
        self._logger.info(f"publish_to_peer: {GLOBAL_TOPIC}/{self.peer_id}")
        packet = ipfs_utils.create_data_packet(
            self.bootstrapper.get_signing_key(),
            self.bootstrapper.get_verify_key(),
            sender=self.bootstrapper.get_verify_key().hex(),
            nonce=os.urandom(32).hex(),
            DH=self.my_DH_keys[1].hex(),
        )
        self.publisher.publish(f"{GLOBAL_TOPIC}/{self.peer_id}", packet)
        self.machine.send("message_published")

    def on_open_connection(self):
        self._logger.info(f"open_connection")

        connection_utils.create_connection_threaded(
            connectionHandler=self.bootstrapper.connectionHandler,
            my_private_DH=self.my_DH_keys[0].hex(),
            peer_public_DH=self.peer_DH_public.hex(),
            peer_id=ipfs_utils.public_key_to_id(bytes.fromhex(self.peer_id)),
            peer_verify_key=self.peer_id,
            is_server=True,
        )
        self.machine.send("connection_started")


class BootstrapperMachine(state_machine.MachineBase):
    def __init__(
        self, bootstrapper, log_state_entries=state_machine.DEFAULT_LOG_STATE_ENTRIES
    ):
        self.name = MachineNames.Bootstrapper.value

        self.states = [
            {
                "name": "initialization",
                "entry": "get_or_create_ed25519_keys()",
            },
            {
                "name": "no_peers",
                "entry": f"send_to('start', '{MachineNames.GlobalListener.value}')",
            },
            {
                "name": "bootstrapped",
                "entry": f'send_to("start", "{MachineNames.PrivateListener.value}"); send_to("start", "{MachineNames.GlobalAdvertiser.value}")',
            },
        ]

        self.transitions = [
            {
                "source": "initial",
                "target": "initialization",
            },
            {
                "trigger": "start",
                "source": "initialization",
                "target": "no_peers",
            },
            {
                "trigger": "no_global_activity",
                "source": "no_peers",
                "target": "bootstrapped",
            },
            {
                "trigger": "connected",
                "source": "no_peers",
                "target": "bootstrapped",
            },
        ]

        super().__init__(bootstrapper, log_state_entries=log_state_entries)

    def get_or_create_ed25519_keys(self):
        self._logger.info(f"get_or_create_ed25519_keys")
        time.sleep(0.5)  # wait for machines to start
        signing_key, verify_key = crypto.get_or_create_key_pair_file(type="ed25519")
        nonce = os.urandom(32)
        singed_nonce = crypto.sign_data(signing_key, bytes(nonce))
        is_verified = crypto.verify_signature(verify_key, singed_nonce, bytes(nonce))
        if is_verified:
            self.bootstrapper.set_ed25519_keys(signing_key, verify_key)
            self.machine.send("start")
        else:
            self.get_or_create_ed25519_keys()


class PrioritySet(object):
    def __init__(self):
        self.heap = []
        self.set = set()

    def add(self, d, pri):
        if not d in self.set:
            heapq.heappush(self.heap, (pri, d))
            self.set.add(d)

    def pop(self):
        _, d = heapq.heappop(self.heap)
        self.set.remove(d)
        return d


class Bootstrapper(object):
    def __init__(
        self, device_id, wait_for: int = None, started_at=datetime.datetime.now()
    ):
        self.drivers = [Driver(), Driver(), Driver(), Driver()]
        self.machines = {}
        self.public_keys = PrioritySet()
        self.connected_peers = {}
        self.connectionHandler = con.ConnectionHandler()
        self.device = BootstrapperMachine(self, log_state_entries=True)
        self.id = None
        self.global_listener_wait_for = wait_for
        self.device_id = device_id
        self.started_at = started_at
        self.started = False
        self.machine = self.device.machine
        self._logger = logging.getLogger(__name__)
        self.drivers[0].add_machine(self.machine)
        self.drivers[0].add_machine(
            PrivateListener(self, log_state_entries=True).get_machine()
        )
        self.drivers[1].add_machine(
            GlobalListener(self, log_state_entries=True).get_machine()
        )
        self.drivers[1].add_machine(
            Connecter(self, log_state_entries=False).get_machine()
        )
        self.drivers[2].add_machine(
            GlobalAdvertiser(self, log_state_entries=False).get_machine()
        )

    def start(self):
        if self.started:
            self._logger.error("Bootstrapper already started")
            return
        for driver in self.drivers:
            driver.start()
        self.stated = True
        self._logger.info("startet Bootstrapper")

    def stop(self):
        self._logger.info("stopping Bootstrapper")
        if not self.started:
            self._logger.error("Bootstrapper not started")
            return
        for driver in self.drivers:
            driver.stop()

    def set_id(self, id: int):
        self.connectionHandler.set_id(int(id))

    def set_ed25519_keys(self, private_key: bytes, public_key: bytes):
        print(f"My signing_key:\t{private_key.hex()}")
        print(f"My verify_key:\t\t{public_key.hex()}")
        self.signing_key = private_key
        self.public_key = public_key
        self.set_id(int(public_key.hex(), 16) % 255)

    def get_signing_key(self) -> bytes:
        return self.signing_key

    def get_verify_key(self) -> bytes:
        return self.public_key

    def check_if_machine_exists(self, machine_name):
        for driver in self.drivers:
            if machine_name in driver._stms_by_id.keys():
                return True
        return False

    def send_to(self, event, machine_name):
        self._logger.info(f"send_to({event}, {machine_name})")
        for driver in self.drivers:
            if machine_name in driver._stms_by_id.keys():
                return driver.send(event, machine_name)
        self._logger.error(f"machine driver not found for {machine_name}")

    def add_public_key(self, public_key: bytes):
        self.public_keys.add(public_key, 0)

    def get_first_public_key(self) -> bytes:
        return self.public_keys.pop()

    def create_peer_channel_machine(self, public_key: bytes):
        if self.check_if_machine_exists(
            f"{MachineNames.PeerChannel.value}_{public_key.hex()}"
        ):
            return
        self.drivers[3].add_machine(PeerChannel(self, public_key.hex()).get_machine())

    def get_want_new_connection(self):
        return self.connectionHandler.want_new_connection


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--device", help="device_id", type=int, required=True)
    parser.add_argument("-w", "--wait_for", help="device_id", type=int)
    return parser.parse_args()


if __name__ == "__main__":
    args = _parse_args()
    if args.device is None:
        print("please provide device id")
        exit(1)
    wait_for = None if not args.wait_for else args.wait_for

    now = datetime.datetime.now()
    print("starting bootstrapper at ", now)
    utils.update_torpy_consensus_if_necessary()
    try:
        bootstrapper = Bootstrapper(args.device, wait_for, started_at=now)
        bootstrapper.start()
    except KeyboardInterrupt:
        bootstrapper.stop()
        print("bootstrapper stopped")
