import io
import json
from math import inf
from platform import python_version
import ssl
import sys
import threading
import time
import traceback
import requests
from subprocess import call
import multibase
import logging
import urllib3

logger = logging.getLogger(__name__)
logging.getLogger("urllib3").setLevel(logging.CRITICAL)
logger.setLevel(level=logging.CRITICAL)


def bootstrappingCallback(data: str, seqno: int, topic_ids: list, cid: str):
    print(f"{','.join(topic_ids)}: {data}")


def encode_topic(topic: str):
    return multibase.encode("base64url", topic).decode("utf8")


class BaseStream:
    def __init__(self, *, chunk_size=512, daemon=False, max_retries=inf, verify=True):
        self.chunk_size = chunk_size
        self.daemon = daemon
        self.max_retries = max_retries
        self.verify = verify

        self.running = False
        self.session = requests.Session()
        self.thread = None
        self.user_agent = (
            f"Python/{python_version()} " f"Requests/{requests.__version__} "
        )

    def _connect(self, method, url, params=None, body=None, timeout=21):
        self.running = True

        error_count = 0
        network_error_wait = 0
        network_error_wait_step = 0.25
        network_error_wait_max = 16
        http_error_wait = http_error_wait_start = 5
        http_error_wait_max = 320
        http_429_error_wait_start = 60

        self.session.headers["User-Agent"] = self.user_agent

        try:
            while self.running and error_count <= self.max_retries:
                try:
                    with self.session.request(
                        method,
                        url,
                        params=params,
                        data=body,
                        timeout=timeout,
                        stream=True,
                        verify=self.verify,
                    ) as resp:
                        if resp.status_code == 200:
                            error_count = 0
                            http_error_wait = http_error_wait_start
                            network_error_wait = 0

                            self.on_connect()
                            if not self.running:
                                break

                            for line in resp.iter_lines(chunk_size=self.chunk_size):
                                if line:
                                    self.on_data(line)
                                else:
                                    self.on_keep_alive()
                                if not self.running:
                                    break

                            if resp.raw.closed:
                                self.on_closed(resp)
                        else:
                            self.on_request_error(resp.status_code)
                            if not self.running:
                                break
                            # The error text is logged here instead of in
                            # on_request_error to keep on_request_error
                            # backwards-compatible. In a future version, the
                            # Response should be passed to on_request_error.
                            logger.error("HTTP error response text: %s", resp.text)

                            error_count += 1

                            if resp.status_code in (420, 429):
                                if http_error_wait < http_429_error_wait_start:
                                    http_error_wait = http_429_error_wait_start

                            time.sleep(http_error_wait)

                            http_error_wait *= 2
                            if http_error_wait > http_error_wait_max:
                                http_error_wait = http_error_wait_max
                except (
                    requests.ConnectionError,
                    requests.Timeout,
                    requests.exceptions.ChunkedEncodingError,
                    ssl.SSLError,
                    urllib3.exceptions.ReadTimeoutError,
                    urllib3.exceptions.ProtocolError,
                ) as exc:
                    # This is still necessary, as a SSLError can actually be
                    # thrown when using Requests
                    # If it's not time out treat it like any other exception
                    if isinstance(exc, ssl.SSLError):
                        if not (exc.args and "timed out" in str(exc.args[0])):
                            raise

                    self.on_connection_error()
                    if not self.running:
                        break
                    # The error text is logged here instead of in
                    # on_connection_error to keep on_connection_error
                    # backwards-compatible. In a future version, the error
                    # should be passed to on_connection_error.
                    logger.error(
                        "Connection error: %s",
                        "".join(
                            traceback.format_exception_only(type(exc), exc)
                        ).rstrip(),
                    )

                    time.sleep(network_error_wait)

                    network_error_wait += network_error_wait_step
                    if network_error_wait > network_error_wait_max:
                        network_error_wait = network_error_wait_max
        except Exception as exc:
            self.on_exception(exc)
        finally:
            self.session.close()
            self.running = False
            self.on_disconnect()

    def _threaded_connect(self, *args, **kwargs):
        self.thread = threading.Thread(
            target=self._connect,
            name="datastream",
            args=args,
            kwargs=kwargs,
            daemon=self.daemon,
        )
        self.thread.start()
        return self.thread

    def disconnect(self):
        """Disconnect the stream"""
        self.running = False

    def on_closed(self, response):
        """This is called when the stream has been closed by Server.
        Parameters
        ----------
        response : requests.Response
            The Response from Server
        """
        logger.error("Stream connection closed by Server")

    def on_connect(self):
        """This is called after successfully connecting to the streaming API."""
        logger.info(f"Stream connected on ")

    def on_connection_error(self):
        """This is called when the stream connection errors or times out."""
        # logger.error(f"Stream connection has errored or timed out")

    def on_disconnect(self):
        """This is called when the stream has disconnected."""
        logger.info(f"Stream disconnected")

    def on_exception(self, exception):
        """This is called when an unhandled exception occurs.
        Parameters
        ----------
        exception : Exception
            The unhandled exception
        """
        logger.exception(f"Stream encountered an exception")

    def on_keep_alive(self):
        """This is called when a keep-alive signal is received."""
        logger.debug(f"Received keep-alive signal")

    def on_request_error(self, status_code):
        """This is called when a non-200 HTTP status code is encountered.
        Parameters
        ----------
        status_code : int
            The HTTP status code encountered
        """
        logger.error("Stream encountered HTTP error: %d", status_code)


class IPFSSubscriptionStream(BaseStream):
    def __init__(self, topic, callback, endpoint="/dns/127.0.0.1/tcp/5001/http"):
        self.topic = topic
        self.callback = callback
        ip, port = self._parse_endpoint(endpoint)
        self.baseUrl = f"http://{ip}:{port}/api/v0/pubsub"
        super().__init__(daemon=True)

    def start(self):
        self._threaded_connect(
            "post",
            f"{self.baseUrl}/sub?arg={encode_topic(self.topic)}",
        )
        logger.info(f"subscribed to {self.topic}, {encode_topic(self.topic)}")

    def stop(self):
        self.disconnect()
        logger.info(f"unsubscribed from {self.topic}, {encode_topic(self.topic)}")
        self.thread.join()

    def on_data(self, data):
        try:
            j = json.loads(data.decode("utf8"))
        except:
            logger.error("could not decode json")
            return
        content = ""
        try:
            content = self._decode_data(j.get("data", ""))
        except Exception as e:
            logger.error("could not decode data", e)
        try:
            j["seqno"] = int.from_bytes(multibase.decode((j["seqno"])), "big")
        except:
            logger.error("could not decode seqno")

        for i in range(len(j["topicIDs"])):
            j["topicIDs"][i] = multibase.decode(j["topicIDs"][i]).decode("utf8")

        self.callback(
            data=content,
            seqno=j["seqno"],
            topic_ids=j["topicIDs"],
            cid=j["from"],
        )

    def _decode_data(self, data: str):
        data_bytes = multibase.decode(data)
        return data_bytes.decode("utf8")

    def _parse_endpoint(self, endpoint: str):
        logger.info("Parsing endpoint: " + endpoint)
        ip = endpoint.split("/dns/")[1].split("/")[0]
        port = int(endpoint.split("/tcp/")[1].split("/")[0])
        return ip, port


class IPFSPublisher:
    def __init__(self, endpoint="/dns/127.0.0.1/tcp/5001/http"):
        ip, port = self._parse_endpoint(endpoint)
        self.baseUrl = f"http://{ip}:{port}/api/v0/pubsub"

    def publish(self, topic, data):
        logger.info(f"Publishing to topic: {topic}")
        file = io.BytesIO(data if type(data) == bytes else data.encode("utf8"))
        files = {"file": file}
        requests.post(
            f"{self.baseUrl}/pub?arg={encode_topic(topic)}",
            files=files,
        )

    def _parse_endpoint(self, endpoint: str):
        logger.info("Parsing endpoint: " + endpoint)
        ip = endpoint.split("/dns/")[1].split("/")[0]
        port = int(endpoint.split("/tcp/")[1].split("/")[0])
        return ip, port


class IPFSPubSub:
    def __init__(
        self, id, global_topic="bootstrapping", endpoint="/dns/127.0.0.1/tcp/5001/http"
    ):
        self.global_topic = global_topic
        self.id = id
        self.subscriptions = {}
        self.endpoint = endpoint
        self.publish_every = {}
        if not self._is_IPFS_installed:
            raise Exception("IPFS is not installed")

    def _subscribe(self, topic):
        if not topic in self.subscriptions.keys():
            subStream = IPFSSubscriptionStream(
                topic,
                bootstrappingCallback,
                self.endpoint,
            )
            self.subscriptions[topic] = [[], subStream]
            subStream.start()
        print(f"---Subscribed to topic---: {topic} {encode_topic(topic)}")

    def subscribe_global(self):
        self._subscribe(self.global_topic)

    def unsubscribe(self, topic):
        print(f"Unsubscribing to topic: {topic}")
        if topic in self.subscriptions.keys():
            for i in range(len(self.subscriptions[topic][0]) - 1, -1, -1):
                if not self.subscriptions[topic][0][i] is None:
                    self.subscriptions[topic][0][i].stop()
                    self.subscriptions[topic][0][i] = None
            self.subscriptions[topic][1] = False

    def unsubscribe_all(self):
        print("unsubscribing all", self.subscriptions.keys())
        for subStream in self.subscriptions.values():
            print("unsubscribing", subStream)
            subStream[1].stop()

    def publish_every(self, topic: str, data, sec: float):
        if not topic in self.publish_every.keys():
            t = threading.Timer(sec, self.publish_every, [topic, data])
            self.publish_every[topic] = t
            t.start()
            self.publish(topic, data)

    def stop_publish_every(self, topic: str):
        if topic in self.publish_every.keys():
            self.publish_every[topic].cancel()
            logger.info(f"Stopped publishing to topic: {topic}")

    def publish(self, topic, data):
        logger.info(f"Publishing to topic: {topic}")
        file = io.BytesIO(data if type(data) == bytes else data.encode("utf8"))
        files = {"file": file}
        requests.post(
            f"{self.baseUrl}/pub?arg={encode_topic(topic)}",
            files=files,
        )

    def _is_IPFS_installed(self) -> bool:
        try:
            return_value = call(["ipfs", "--version"])
            if return_value != 0:
                return False
            return True
        except:
            return False


if __name__ == "__main__":
    client = IPFSPubSub(id="my_id", global_topic="bootstrapping")

    client.subscribe_global()
    while True:
        try:
            time.sleep(10)
        except KeyboardInterrupt:
            client.unsubscribe_all()
            sys.exit()
