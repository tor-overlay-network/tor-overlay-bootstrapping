import json
import subprocess
import src.bootstrapping.crypto as crypto


def start_ipfs():
    return subprocess.Popen(
        [
            "ipfs",
            "daemon",
            "--enable-pubsub-experiment",
        ],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )


def public_key_to_id(public_key: bytes):
    return int(public_key.hex(), 16) % 255


def create_data_packet(signing_key: bytes, verify_key: bytes, **data):
    packet = {}
    for key, value in data.items():
        if type(value) == bytes:
            packet[key] = value.hex()
        else:
            packet[key] = value
    h = crypto.get_hash_from_data(packet)
    signature = crypto.sign_data(signing_key, h)
    if not crypto.verify_signature(verify_key, signature, h):
        print("Signature verification failed")
    packet["sign"] = signature.hex()
    return json.dumps(packet)


def get_verfied_data_from_packet(packet: str):
    data = json.loads(packet)
    try:
        signature = bytes.fromhex(data["sign"])
        del data["sign"]
        sender_packet = bytes.fromhex(data["sender"])
    except KeyError:
        raise ValueError("Packet does not contain signature")
    h = crypto.get_hash_from_data(data)
    is_verified = crypto.verify_signature(sender_packet, signature, h)
    if not is_verified:
        raise ValueError("Packet signature verification failed")
    return data
