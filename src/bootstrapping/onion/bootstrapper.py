import argparse
import datetime
from enum import Enum
import logging
import random
import threading


from stmpy import Driver
from requests_tor import RequestsTor

import src.bootstrapping.onion.onion_utils as onion_utils
import src.bootstrapping.state_machine as state_machine
import src.bootstrapping.crypto as crypto
import src.bootstrapping.onion.group_onion as group_onion
import src.bootstrapping.onion.private_onion as private_onion


import src.routing.connection as con
import src.routing.utils as connection_utils

logging.getLogger("stmpy").setLevel(logging.CRITICAL)


class MachineNames(Enum):
    Renewer = "renewer"
    Connecter = "connecter"
    Bootstrapper = "bootstrapper"


class RenewerMachine(state_machine.MachineBase):
    def __init__(
        self, bootstrapper, log_state_entries=state_machine.DEFAULT_LOG_STATE_ENTRIES
    ):
        self.name = MachineNames.Renewer.value

        self.states = [
            {
                "name": "idle",
            },
            {
                "name": "timer",
                "entry": f"start_timer('t', {5*60*1000})",  # 5 minutes
                "exit": "stop_timer('t')",
            },
        ]

        self.transitions = [
            {
                "source": "initial",
                "target": "idle",
            },
            {
                "trigger": "start",
                "source": "idle",
                "target": "timer",
            },
            {
                "trigger": "t",
                "source": "timer",
                "target": "timer",
                "effect": "renew_registration()",
            },
            {
                "trigger": "stop",
                "source": "timer",
                "target": "idle",
            },
        ]

        super().__init__(bootstrapper, log_state_entries=log_state_entries)

    def renew_registration(self):
        print("renew_registration")
        self.bootstrapper.register_public_key()


class ConnecterMachine(state_machine.MachineBase):
    def __init__(
        self, bootstrapper, log_state_entries=state_machine.DEFAULT_LOG_STATE_ENTRIES
    ):
        self.name = MachineNames.Connecter.value

        self.states = [
            {
                "name": "idle",
            },
            {
                "name": "update",
                "entry": f"start_timer('participants', {2*60*1000}) ; create_connections_if_possible()",
                "exit": "stop_timer('participants')",
            },
        ]

        self.transitions = [
            {
                "source": "initial",
                "target": "idle",
            },
            {
                "trigger": "start",
                "source": "idle",
                "target": "update",
                "effect": "update_participants_list()",
            },
            {
                "trigger": "participants",
                "source": "update",
                "target": "update",
                "effect": "update_participants_list()",
            },
            {
                "trigger": "group_onion_down",
                "source": "update",
                "target": "idle",
                "effect": f"send_to('group_onion_down', '{MachineNames.Bootstrapper.value}')",
            },
        ]

        super().__init__(bootstrapper, log_state_entries=log_state_entries)

    def update_participants_list(self):
        print("update_participants_list")
        rt = RequestsTor(tor_ports=(9050,), tor_cport=9051)
        url = f"http://{self.bootstrapper.group_onion_address}/participants"
        try:
            response = rt.get(url)
            data = response.json()
            self.bootstrapper.update_local_participants_list(data)
        except Exception as e:
            print("Unable to get participants at: ", url, e)
            self.machine.send("group_onion_down")

    def create_connections_if_possible(self):
        print("create_connections_if_possible")
        connectionHandler: con.ConnectionHandler = self.bootstrapper.connectionHandler
        if not connectionHandler.can_add_new_connection:
            print("cannot add new connections, is full")
            return
        local_participants_list = self.bootstrapper.get_local_participants_list()
        if local_participants_list is None or len(local_participants_list) == 0:
            print("local_participants_list is empty")
            self.machine.send("participants")
            return
        random.shuffle(local_participants_list)
        for possible_peer in local_participants_list:
            if not connectionHandler.want_new_connection:
                print("dont't want new connections")
                break
            if int(possible_peer[0]) == int(connectionHandler.id):
                continue
            if connectionHandler.is_connected_to(int(possible_peer[0])):
                continue
            private_DH, public_DH = crypto.generate_new_raw_key_pair("x25519")
            private_keys: crypto.ED25519KeyHandler = self.bootstrapper.private_keys
            request_body = private_onion.create_connect_request(
                signing_key=private_keys.signing_key_bytes,
                verify_key=private_keys.verify_key_bytes,
                public_DH_key=public_DH,
                id=int(connectionHandler.id),
            )
            try:
                peer_public_key = bytes.fromhex(possible_peer[1])
                peer_onion_address = onion_utils.onion_address_from_public_key(
                    peer_public_key
                )
                url = f"http://{peer_onion_address}/connect"
                rt = RequestsTor(tor_ports=(9050,), tor_cport=9051)
                r = rt.post(url, json=request_body)
                data = r.json()
                print("response ", data)
            except Exception as e:
                print("Could not connect to: ", f"{possible_peer[1]}")
                continue
            if int(data.get("id", -1)) != int(possible_peer[0]):
                print("Peer id does not match")
                continue
            try:
                connection_utils.create_connection(
                    connectionHandler,
                    my_private_DH=private_DH.hex(),
                    peer_public_DH=data["DH"],
                    peer_id=int(data["id"]),
                    peer_verify_key=data["signer"],
                    is_server=False,
                )
            except Exception as e:
                print("Could not create connection: ", e)
                continue
        print("loop through participants list finished ")


class BootstrapperMachine(state_machine.MachineBase):
    def __init__(
        self,
        bootstrapper,
        log_state_entries=state_machine.DEFAULT_LOG_STATE_ENTRIES,
    ):
        self.name = MachineNames.Bootstrapper.value
        self.states = [
            {
                "name": "initialization",
                "entry": "get_or_create_private_ed25519_keys();",
            },
            {
                "name": "check_group_onion",
                "entry": f"check_if_group_onion_up()",
            },
            {
                "name": "setup_group_onion",
                "entry": f"start_group_onion()",
            },
            {"name": "unregistered", "entry": "register_public_key()"},
            {
                "name": "registered",
                "entry": f"send_to('start', '{MachineNames.Connecter.value}'); start_private_onion(); send_to('start', '{MachineNames.Renewer.value}')",
            },
        ]

        self.transitions = [
            {
                "source": "initial",
                "target": "initialization",
            },
            {
                "trigger": "start",
                "source": "initialization",
                "target": "check_group_onion",
            },
            {
                "trigger": "stop",
                "source": "check_group_onion",
                "target": "initialization",
                "effect": "end_stm()",
            },
            {
                "trigger": "group_onion_up",
                "source": "check_group_onion",
                "target": "unregistered",
            },
            {
                "trigger": "group_onion_down",
                "source": "check_group_onion",
                "target": "setup_group_onion",  # TODO add timer here
            },
            {
                "trigger": "success",
                "source": "setup_group_onion",
                "target": "unregistered",
            },
            {
                "trigger": "already_up",
                "source": "setup_group_onion",
                "target": "unregistered",
            },
            {
                "trigger": "registered",
                "source": "unregistered",
                "target": "registered",
            },
        ]

        super().__init__(bootstrapper, log_state_entries=log_state_entries)

    def get_or_create_private_ed25519_keys(self):
        self._logger.info(f"get_or_create_ed25519_keys")
        signing_key, verify_key = crypto.get_or_create_key_pair_file(
            self.bootstrapper.device_id, type="ed25519"
        )
        try:
            self.bootstrapper.set_private_ed25519_keys(signing_key, verify_key)
            self.machine.send("start")
        except Exception:
            self.get_or_create_private_ed25519_keys()

    def check_if_group_onion_up(self):
        self._logger.info(f"check_if_group_onion_up")
        if self.bootstrapper.group_onion_address is None:
            self._logger.warn("group_onion_address is not set")
            return False
        is_onion_running = onion_utils.check_if_onion_server_is_running(
            self.bootstrapper.group_onion_address
        )
        if is_onion_running:
            self.machine.send("group_onion_up")
            return True
        else:
            self.machine.send("group_onion_down")
            return False

    def register_public_key(self):
        self.bootstrapper.register_public_key()

    def start_group_onion(self):
        self._logger.info(f"start_group_onion")
        """  is_onion_running = onion_utils.check_if_onion_server_is_running(
            self.bootstrapper.group_onion
        )
        if is_onion_running:
            self.machine.send("already_up")
            return """

        self.bootstrapper.group_server_event = threading.Event()
        self.bootstrapper.group_onion = group_onion.start_group_onion_threaded(
            self.bootstrapper.group_keys.signing_key_bytes,
            self.bootstrapper.private_keys.signing_key_bytes,
            self.bootstrapper.group_server_event,
        )
        self.machine.send("success")

    def start_private_onion(self):
        self._logger.info(f"start_private_onion")
        if (
            self.bootstrapper.private_server_thread is not None
            and self.bootstrapper.private_server_thread.is_alive()
        ):
            self._logger.warn("private_server_thread is already running")
            return
        self.bootstrapper.private_server_event = threading.Event()
        self.bootstrapper.private_server_thread = (
            private_onion.start_private_onion_threaded(
                self.bootstrapper.private_keys.signing_key_bytes,
                self.bootstrapper.connectionHandler,
                self.bootstrapper.private_server_event,
            )
        )


class Bootstrapper(object):
    def __init__(self, secret: str, device_id: int, started_at=datetime.datetime.now()):
        self.drivers = [Driver(), Driver(), Driver()]
        self.machines = {}
        self.connected_peers = {}
        self.connections = {}
        self.device_id = device_id
        self.device = BootstrapperMachine(self)
        self.started_at = started_at
        self.group_secret = secret
        self.group_keys = crypto.ED25519KeyHandler()
        self.private_keys = crypto.ED25519KeyHandler()
        self.participants_list = []
        self.private_server_thread = None
        self.private_server_event = None
        self.group_server_process = None
        self.group_server_event = None
        self.group_onion_address = None
        self.started = False
        self.machine = self.device.machine
        self.connectionHandler = con.ConnectionHandler(
            polling_interval=15, created_at=started_at
        )
        self._logger = logging.getLogger(__name__)
        self.drivers[0].add_machine(self.machine)
        self.drivers[1].add_machine(ConnecterMachine(self).get_machine())
        self.drivers[2].add_machine(RenewerMachine(self).get_machine())
        self.set_group_keys(secret)

    def start(self):
        if self.started:
            self._logger.error("Bootstrapper already started")
            return
        for driver in self.drivers:
            driver.start()
        self.stated = True
        self._logger.info("startet Bootstrapper")

    def stop(self):
        self._logger.info("stopping Bootstrapper")
        if not self.started:
            self._logger.error("Bootstrapper not started")
            return
        self.group_server_event.set()
        self.private_server_event.set()
        for driver in self.drivers:
            driver.stop()

    def set_id(self, id: int):
        self.connectionHandler.set_id(int(id))

    def _set_group_onion_address(self, verify_key: bytes) -> str:
        self.group_onion_address = onion_utils.onion_address_from_public_key(verify_key)
        print("group_onion_address", self.group_onion_address)
        return self.group_onion_address

    def set_group_keys(self, secret: str) -> crypto.ED25519KeyHandler:
        print("set_group_keys from secret", secret)
        keyhandler = self.group_keys.set_keys_from_secret(secret.encode("utf-8"))
        self._set_group_onion_address(keyhandler.verify_key_bytes)
        return keyhandler

    def get_group_keys(self) -> crypto.ED25519KeyHandler:
        return self.group_keys

    def set_private_ed25519_keys(
        self, signing_key: bytes, verify_key: bytes
    ) -> crypto.ED25519KeyHandler:
        return self.private_keys.set_keys(signing_key, verify_key)

    def get_private_keys(self) -> crypto.ED25519KeyHandler:
        return self.private_keys

    def get_local_participants_list(self) -> list:
        return self.participants_list.copy()

    def update_local_participants_list(self, participants_list: list):
        print(
            "update_local_participants_list", participants_list, type(participants_list)
        )
        self.participants_list = participants_list

    def send_to(self, event, machine_name):
        self._logger.info(f"send_to({event}, {machine_name})")
        for driver in self.drivers:
            if machine_name in driver._stms_by_id.keys():
                return driver.send(event, machine_name)
        self._logger.error(f"machine driver not found for {machine_name}")

    def register_public_key(self):
        private_keys: crypto.ED25519KeyHandler = self.get_private_keys()
        is_onion_running = onion_utils.check_if_onion_server_is_running(
            self.group_onion_address
        )
        print("is_onion_running", is_onion_running)
        request_content, url = group_onion.create_register_request(
            signing_key=private_keys.signing_key_bytes,
            verify_key=private_keys.verify_key_bytes,
            onion_address=self.group_onion_address,
        )
        rt = RequestsTor(tor_ports=(9050,), tor_cport=9051)
        try:
            response1 = rt.get(f"http://{self.group_onion_address}/")
            print("response1", response1.json())
            response2 = rt.post(url, json=request_content)
            data = response2.json()
            my_id = data.get("id", None)
            if my_id is not None:
                self.set_id(my_id)
                self.machine.send("registered")
        except Exception as e:
            print("Unable to register at: ", url, e)
            self.machine.send("register_failed")


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s", "--secret", help="secret used to create net", type=str, required=True
    )
    parser.add_argument("-d", "--device", help="device_id", type=int, required=True)
    return parser.parse_args()


if __name__ == "__main__":
    args = _parse_args()
    if args.secret is None or args.device is None:
        print("please provide secret and device id")
        exit(1)
    started_at = datetime.datetime.now()
    print("starting bootstrapper at ", started_at)
    bootstrapper = Bootstrapper(args.secret, args.device, started_at=started_at)
    bootstrapper.start()
