import argparse
import datetime
import os
import shutil
import sqlite3
import json
import threading
from typing import Tuple
from marshmallow import Schema, fields, ValidationError

from stem.control import Controller
from stem import ControllerError, Signal, DescriptorUnavailable
from flask import Flask, jsonify, request

import src.bootstrapping.utils as utils
import src.bootstrapping.onion.onion_utils as onion_utils
import src.bootstrapping.crypto as crypto

app = Flask(__name__)

FLASK_PORT = 5006


class ReqisterSchema(Schema):
    public_key = fields.String(required=True)
    nonce = fields.String(required=True)
    sign = fields.String(required=True)


def get_db_filepath(db_name: str):
    if not os.path.exists("db"):
        os.makedirs("db")
    return f"db/{db_name}.db"


def create_db(name):
    c = sqlite3.connect(get_db_filepath(name)).cursor()
    c.execute(
        "CREATE TABLE IF NOT EXISTS PARTICIPANTS("
        "id TEXT, public_key TEXT NOT NULL UNIQUE, created_at TIMESTAMP, valid_until TIMESTAMP)"
    )
    c.connection.close()


def get_participant_by_public_key(public_key):
    db = sqlite3.connect(get_db_filepath(app.config.get("db_name")))
    db.row_factory = sqlite3.Row
    c = db.cursor()
    c.execute("SELECT * FROM PARTICIPANTS WHERE public_key = ?", (public_key,))
    participant = c.fetchone()
    c.connection.close()
    return dict(zip(participant.keys(), participant))


def list_current_participants():
    c = sqlite3.connect(get_db_filepath(app.config.get("db_name"))).cursor()
    c.execute(
        "SELECT * FROM PARTICIPANTS WHERE valid_until > datetime('now') ORDER BY valid_until ASC"
    )
    return c.fetchall()


def add_or_update_participant_in_db(public_key):
    id = get_next_available_id()
    created_at = datetime.datetime.now()
    valid_until = created_at + datetime.timedelta(
        minutes=app.config.get("registration_valid_minutes")
    )
    db = sqlite3.connect(get_db_filepath(app.config.get("db_name")))
    db.row_factory = sqlite3.Row
    c = db.cursor()
    c.execute(
        "INSERT OR IGNORE INTO PARTICIPANTS VALUES(?, ?, ?, ?)",
        (id, public_key, created_at, valid_until),
    )
    c.execute(
        "UPDATE PARTICIPANTS SET valid_until = ? WHERE public_key = ?",
        (valid_until, public_key),
    )
    db.commit()

    return get_participant_by_public_key(public_key)


def get_next_available_id():
    db = sqlite3.connect(get_db_filepath(app.config.get("db_name")))
    c = db.cursor()
    c.execute("SELECT MAX(id) FROM PARTICIPANTS")
    max_id = c.fetchone()[0]
    c.connection.close()
    if max_id is None:
        return 0
    return int(max_id) + 1


def create_register_response(
    public_key: str,
    id: int,
    created_at: datetime,
    valid_until: datetime,
    signer: bytes,
    signing_key: bytes,
):
    response = {
        "public_key": public_key,
        "id": id,
        "created_at": created_at,
        "valid_until": valid_until,
        "signer": signer.hex(),
    }
    return crypto.add_signature_to_data(signing_key, signer, response)


def create_register_request(
    signing_key: bytes,
    verify_key: bytes,
    onion_address: str,
) -> Tuple[dict, str]:
    url_http = f"http://{onion_address}/register"
    content = {
        "public_key": verify_key.hex(),
        "nonce": os.urandom(32).hex(),
    }
    h = crypto.get_hash_from_data(content)
    sign_h = crypto.sign_data(signing_key, h)
    is_verified = crypto.verify_signature(verify_key, sign_h, h)
    if not is_verified:
        raise Exception("sign is not valid")
    content["sign"] = sign_h.hex()
    return content, url_http


@app.route("/", methods=["GET"])
def index():
    uptime = datetime.datetime.utcnow() - app.config["started_at_utc"]
    private_keys: crypto.ED25519KeyHandler = app.config.get("private_keys")
    response = {
        "network": app.config.get("group_keys").verify_key_hex,
        "uptime": str(uptime),
        "started_at_utc": app.config.get("started_at_utc"),
        "signer": private_keys.verify_key_hex,
    }
    return crypto.add_signature_to_data(
        private_keys.signing_key_bytes, private_keys.verify_key_bytes, response
    )


@app.route("/register", methods=["POST"])
def register():
    content = request.get_json(silent=False, force=True)
    schema = ReqisterSchema()
    try:
        result = utils.validate_response(schema, content)
    except ValidationError as err:
        return jsonify(err.messages), 422

    print("register request", result)
    try:
        db_data = add_or_update_participant_in_db(result.get("public_key"))
        print("db_data", db_data)
    except sqlite3.IntegrityError:
        return jsonify({"error": "public key already registered"}), 400
    private_keys: crypto.ED25519KeyHandler = app.config.get("private_keys")
    response = create_register_response(
        public_key=db_data.get("public_key"),
        id=db_data.get("id"),
        created_at=db_data.get("created_at"),
        valid_until=db_data.get("valid_until"),
        signer=private_keys.verify_key_bytes,
        signing_key=private_keys.signing_key_bytes,
    )
    return json.dumps(response, default=str), 200


@app.route("/participants", methods=["GET"])
def list_participants():
    data = list_current_participants()
    return jsonify(data), 200


@app.route("/participants/<public_key>", methods=["GET"])
def get_participant():
    req_args = request.view_args
    data = get_participant_by_public_key(req_args.get("public_key"))
    if data is None:
        return jsonify({"error": "not found"}), 404
    return jsonify(data), 200


def status_callback(*args, **kwargs):
    controller = args[0]
    status = args[1]
    print(f"Controller received status: {status}")


def start_group_onion(
    group_private_key: bytes, my_private_key: bytes, e: threading.Event
) -> None:
    group_keys = crypto.ED25519KeyHandler(group_private_key)
    onion_address = onion_utils.onion_address_from_public_key(
        group_keys.verify_key_bytes
    )
    """ is_onion_running = onion_utils.check_if_onion_server_is_running(onion_address)
    if is_onion_running:
        print(" * Onion server is already running")
        return """

    with Controller.from_port() as controller:
        controller.authenticate()
        controller.signal(
            Signal.NEWNYM
        )  # switch to new circuits, so new application requests don't share any circuits with old ones (this also clears our DNS cache)

        hidden_service_dir = os.path.join(
            controller.get_conf("DataDirectory", "/tmp"),
            onion_address.replace(".onion", ""),
        )
        onion_utils.create_hidden_service_files(
            group_keys.signing_key_bytes, hidden_service_dir
        )
        controller.add_status_listener(status_callback, spawn=True)

        # Create a hidden service where visitors of port 80 get redirected to local
        # port 5000 (this is where Flask runs by default).

        print(" * Creating group hidden service in %s" % hidden_service_dir)
        try:
            result = controller.create_hidden_service(
                hidden_service_dir, 80, target_port=FLASK_PORT
            )
        except ControllerError as exc:
            print(f" * Failed to create hidden service: {exc}")
            print("Exiting...")
            return
        try:
            print("-----------------------------------------")
            descriptor = controller.get_hidden_service_descriptor(
                onion_address.replace(".onion", "")
            )
            print(
                [
                    method_name
                    for method_name in dir(descriptor)
                    if callable(getattr(descriptor, method_name))
                ]
            )
            print("-----------------------------------------")
        except DescriptorUnavailable:
            print(" * Descriptor unavailable")
        # The hostname is only available when we can read the hidden service
        # directory. This requires us to be running with the same user as tor.
        if result.hostname:
            print(
                " * Our service is available at %s, press ctrl+c to quit"
                % result.hostname
            )
        else:
            print(
                " * Unable to determine our service's hostname, probably due to being unable to read the hidden service directory"
            )

        try:
            app.config["onion_address"] = onion_address
            app.config["group_keys"] = group_keys
            app.config["private_keys"] = crypto.ED25519KeyHandler(my_private_key)
            app.config["db_name"] = group_keys.verify_key_hex
            app.config["hidden_service_dir"] = hidden_service_dir
            app.config["registration_valid_minutes"] = 20
            app.config["started_at_utc"] = datetime.datetime.utcnow()

            create_db(app.config.get("db_name"))
            flask_thread = threading.Thread(
                name="flask_group",
                target=lambda: app.run(port=FLASK_PORT, debug=True, use_reloader=False),
            )
            flask_thread.setDaemon(True)
            flask_thread.start()
            print("group onion up at ", datetime.datetime.now())
            while not e.isSet():
                e.wait(1)
        except KeyboardInterrupt:
            pass
        finally:
            print(" * Shutting down our hidden service")
            controller.remove_hidden_service(hidden_service_dir, 80)
            shutil.rmtree(hidden_service_dir)


def start_group_onion_threaded(
    group_private_key: bytes, my_private_key: bytes, e: threading.Event
) -> threading.Thread:
    thread = threading.Thread(
        name="group_onion",
        target=start_group_onion,
        args=(group_private_key, my_private_key, e),
    )
    thread.setDaemon(True)
    thread.start()
    return thread


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s", "--secret", help="relay to be used as rendezvous point", type=str
    )
    parser.add_argument("-i", "--id", help="id used for storing key in file", type=int)
    return parser.parse_args()


if __name__ == "__main__":
    args = _parse_args()
    secret = args.secret if args.secret is not None else "TheSecret"
    my_id = args.id if args.id is not None else 2
    MY_PRIVATE_KEY = crypto.get_or_create_private_key_file(my_id, "ed25519")
    group_keys = crypto.ED25519KeyHandler().set_keys_from_secret(secret.encode("utf-8"))
    e = threading.Event()
    start_group_onion(group_keys.signing_key_bytes, MY_PRIVATE_KEY, e)

    """ thread = start_group_onion_threaded(group_keys.signing_key_bytes, MY_PRIVATE_KEY, e)
    while thread.is_alive():
        try:
            time.sleep(10)
        except KeyboardInterrupt:
            e.set() """
