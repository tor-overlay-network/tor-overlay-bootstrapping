import base64
import datetime
import hashlib
import os
import shutil
from typing import Tuple

from stem import Signal
from stem.control import Controller
from flask import Flask
from nacl.signing import SigningKey

import src.bootstrapping.crypto as crypto
import src.bootstrapping.utils as utils

app = Flask(__name__)


def onion_address_from_public_key(public_key: bytes) -> str:
    version = b"\x03"
    checksum = hashlib.sha3_256(b".onion checksum" + public_key + version).digest()[:2]
    onion_address = "{}.onion".format(
        base64.b32encode(public_key + checksum + version).decode().lower()
    )
    return onion_address


def expand_private_key(secret_key):
    # Based on onionGen's expand_private_key function: https://github.com/rdkr/oniongen-go/blob/master/main.go#L38
    hash = hashlib.sha512(secret_key[:32]).digest()
    hash = bytearray(hash)
    hash[0] &= 248
    hash[31] &= 127
    hash[31] |= 64
    return bytes(hash)


def onion_address_from_secret_key(secret_key: bytes) -> str:
    public_key = crypto.get_public_key(secret_key, "ed25519")
    return onion_address_from_public_key(public_key)


def onion_address_from_signing_key_seed(signing_key_seed: bytes) -> str:
    signing_key = SigningKey(seed=signing_key_seed)
    public_key = bytes(signing_key.verify_key)
    version = b"\x03"
    checksum = hashlib.sha3_256(b".onion checksum" + public_key + version).digest()[:2]
    onion_address = "{}.onion".format(
        base64.b32encode(public_key + checksum + version).decode().lower()
    )
    return onion_address


def get_signing_and_public_key_from_seed(signing_key_seed: bytes) -> bytes:
    signing_key = SigningKey(seed=signing_key_seed)
    public_key = bytes(signing_key.verify_key)
    return signing_key._signing_key, public_key


def verify_v3_onion_address(onion_address: str) -> Tuple[bytes, bytes, bytes]:
    # v3 spec https://gitweb.torproject.org/torspec.git/plain/rend-spec-v3.txt
    try:
        print(f"Checking onion address: {onion_address}")
        decoded = base64.b32decode(onion_address.replace(".onion", "").upper())
        public_key = decoded[:32]
        checksum = decoded[32:34]
        version = decoded[34:]
        print(f"onion public_key:\t {public_key.hex()}")
        print(f"onion checksum:\t {checksum.hex()}")
        print(f"onion version:\t {version.hex()}")

        if (
            checksum
            != hashlib.sha3_256(b".onion checksum" + public_key + version).digest()[:2]
        ):
            raise ValueError
        return public_key, checksum, version
    except:
        raise ValueError("Invalid v3 onion address")


def get_signing_and_public_key_from_hs_ed25519_secret_key(file_content: bytes) -> bytes:
    file_signature = file_content[0:32]
    assert file_signature == b"== ed25519v1-secret: type0 ==\x00\x00\x00"
    file_key_bytes = file_content[32:]
    signing_key = file_key_bytes[:32]
    public_key = file_key_bytes[32:]
    return signing_key, public_key


def create_hs_ed25519_secret_key_content(signing_key: bytes) -> bytes:
    return b"== ed25519v1-secret: type0 ==\x00\x00\x00" + expand_private_key(
        signing_key
    )


def create_hs_ed25519_public_key_content(public_key: bytes) -> bytes:
    assert len(public_key) == 32
    return b"== ed25519v1-public: type0 ==\x00\x00\x00" + public_key


def get_signing_and_public_key_from_seed(signing_key_seed: bytes) -> bytes:
    signing_key = SigningKey(seed=signing_key_seed)
    public_key = bytes(signing_key.verify_key)
    return signing_key._signing_key, public_key


def create_hidden_service_files(private_key: bytes, hidden_service_dir: str) -> None:
    import pwd
    import grp
    from pathlib import Path

    path = Path(hidden_service_dir)
    parent = path.parent.absolute()
    tor_user = parent.owner()
    tor_group = parent.group()
    uid = pwd.getpwnam(tor_user).pw_uid
    gid = grp.getgrnam(tor_group).gr_gid
    if not path.exists():
        # shutil.rmtree(hidden_service_dir)
        os.mkdir(hidden_service_dir)
        os.chmod(hidden_service_dir, 0o700)
        os.chown(hidden_service_dir, uid, gid)

    (
        signing_key,
        verify_key,
    ) = get_signing_and_public_key_from_seed(private_key)
    file_content_secret = create_hs_ed25519_secret_key_content(signing_key)

    secret_file_path = f"{hidden_service_dir}/hs_ed25519_secret_key"
    utils.store_bytes_to_file(file_content_secret, secret_file_path)
    os.chown(secret_file_path, uid, gid)

    public_file_path = f"{hidden_service_dir}/hs_ed25519_public_key"
    file_content_public = create_hs_ed25519_public_key_content(verify_key)
    utils.store_bytes_to_file(file_content_public, public_file_path)
    os.chown(public_file_path, uid, gid)

    hostname_file_path = f"{hidden_service_dir}/hostname"
    onion_address = onion_address_from_public_key(verify_key)
    print(f"onion_address:\t {onion_address}")
    utils.store_string_to_file(onion_address, hostname_file_path)
    os.chown(hostname_file_path, uid, gid)


@app.route("/")
def index():
    return f"<h1>Hi Onion4!</h1><time>The time is{datetime.datetime.now()}</time>"


@app.route("/connect")
def connect():
    return


print(" * Connecting to tor")

with Controller.from_port() as controller:
    controller.authenticate()
    controller.signal(Signal.HUP)

    # All hidden services have a directory on disk. Lets put ours in tor's data
    # directory.

    seed = b"01234567890123456789012345678907"
    signing_key_from_seed, verify_key_from_seed = get_signing_and_public_key_from_seed(
        seed
    )
    onion_address = onion_address_from_public_key(verify_key_from_seed)
    print(
        controller.get_hidden_service_descriptor(
            onion_address.replace(".onion", ""),
            default=f"descriptor for {onion_address.replace('.onion', '')} not found",
        )
    )

    hidden_service_dir = os.path.join(
        controller.get_conf("DataDirectory", "/tmp"),
        onion_address.replace(".onion", ""),
    )
    create_hidden_service_files(seed, hidden_service_dir)

    # Create a hidden service where visitors of port 80 get redirected to local
    # port 5000 (this is where Flask runs by default).

    print(" * Creating our hidden service in %s" % hidden_service_dir)
    result = controller.create_hidden_service(hidden_service_dir, 80, target_port=5000)

    # The hostname is only available when we can read the hidden service
    # directory. This requires us to be running with the same user as tor.

    if result.hostname:
        print(
            " * Our service is available at %s, press ctrl+c to quit" % result.hostname
        )
        """print(
            controller.get_hidden_service_descriptor(
                result.hostname.replace(".onion", ""),
                default=f"descriptor for {result.hostname.replace('.onion', '')} not found",
            )
        )"""
    else:
        print(
            " * Unable to determine our service's hostname, probably due to being unable to read the hidden service directory"
        )

    try:
        app.run()
    finally:
        # Shut down the hidden service and clean it off disk. Note that you *don't*
        # want to delete the hidden service directory if you'd like to have this
        # same *.onion address in the future.

        print(" * Shutting down our hidden service")
        # controller.remove_hidden_service(hidden_service_dir)
        shutil.rmtree(hidden_service_dir)
