import argparse
import sys
import time

from requests_tor import RequestsTor

import src.bootstrapping.onion.private_onion as private_onion
import src.bootstrapping.utils as utils
import src.bootstrapping.crypto as crypto
import src.routing.connection as con
import src.routing.utils as connection_utils


rt = RequestsTor(tor_ports=(9050,), tor_cport=9051)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o", "--onion", help="relay to be used as rendezvous point", type=str
    )
    parser.add_argument(
        "-i",
        "--id",
        help="internal id",
        type=int,
    )
    return parser.parse_args()


if __name__ == "__main__":
    print("Starting client")
    args = parse_args()
    if not args.onion:
        print("Please provide an onion address")
        sys.exit()
    if not args.id:
        print("Please provide an id")
        sys.exit()
    url = f"http://{args.onion}"
    try:
        print("Checking url ", url)
        r = rt.get(url)
        print(r.text)
        print(f"The onion address is valid: {url}")
    except Exception as e:
        print("Could not connect to the url: ", url, e)
        sys.exit()

    my_id = args.id
    print("my_id ", my_id)
    my_private_key = crypto.get_or_create_private_key_file(my_id, "ed25519")
    ed25519_keys = crypto.ED25519KeyHandler(my_private_key)
    assert ed25519_keys.signing_key_bytes == my_private_key
    private_DH, public_DH = crypto.generate_new_raw_key_pair("x25519")
    request_body = private_onion.create_connect_request(
        signing_key=ed25519_keys.signing_key_bytes,
        verify_key=ed25519_keys.verify_key_bytes,
        public_DH_key=public_DH,
        id=my_id,
    )
    retries = 0
    while not utils.update_torpy_consensus_if_necessary() and retries > 3:
        retries += 1
        print("Trying again to update consensus")
    try:
        r = rt.post(f"{url}/connect", json=request_body)
        data = r.json()
        print("response ", data)
    except Exception as e:
        print("Could not connect to the url: ", f"{url}/connect")
        sys.exit()
    connectionHandler = con.ConnectionHandler(id=my_id)
    connection = connection_utils.create_connection(
        connectionHandler,
        my_private_DH=private_DH.hex(),
        peer_public_DH=data["DH"],
        peer_id=int(data["id"]),
        peer_verify_key=data["signer"],
        is_server=False,
    )

    while True:
        try:
            time.sleep(10)
            if not connection.is_process_running() and connection.has_started():
                raise ValueError("Connection is not running")
        except Exception:
            connectionHandler.remove_all_connections()
            sys.exit()
