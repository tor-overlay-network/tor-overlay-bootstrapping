import os
import base64
import hashlib
from typing import Tuple

from requests_tor import RequestsTor
from nacl.signing import SigningKey

import src.bootstrapping.crypto as crypto
import src.bootstrapping.utils as utils


def onion_address_from_public_key(public_key: bytes) -> str:
    version = b"\x03"
    checksum = hashlib.sha3_256(b".onion checksum" + public_key + version).digest()[:2]
    onion_address = "{}.onion".format(
        base64.b32encode(public_key + checksum + version).decode().lower()
    )
    return onion_address


def expand_private_key(secret_key):
    # Based on onionGen's expand_private_key function: https://github.com/rdkr/oniongen-go/blob/master/main.go#L38
    hash = hashlib.sha512(secret_key[:32]).digest()
    hash = bytearray(hash)
    hash[0] &= 248
    hash[31] &= 127
    hash[31] |= 64
    return bytes(hash)


def onion_address_from_secret_key(secret_key: bytes) -> str:
    public_key = crypto.get_public_key(secret_key, "ed25519")
    return onion_address_from_public_key(public_key)


def onion_address_from_signing_key_seed(signing_key_seed: bytes) -> str:
    signing_key = SigningKey(seed=signing_key_seed)
    public_key = bytes(signing_key.verify_key)
    version = b"\x03"
    checksum = hashlib.sha3_256(b".onion checksum" + public_key + version).digest()[:2]
    onion_address = "{}.onion".format(
        base64.b32encode(public_key + checksum + version).decode().lower()
    )
    return onion_address


def get_signing_and_public_key_from_seed(
    signing_key_seed: bytes,
) -> Tuple[bytes, bytes]:
    signing_key = SigningKey(seed=signing_key_seed)
    public_key = bytes(signing_key.verify_key)
    return signing_key._signing_key, public_key


def verify_v3_onion_address(onion_address: str) -> Tuple[bytes, bytes, bytes]:
    # v3 spec https://gitweb.torproject.org/torspec.git/plain/rend-spec-v3.txt
    try:
        print(f"Checking onion address: {onion_address}")
        decoded = base64.b32decode(onion_address.replace(".onion", "").upper())
        public_key = decoded[:32]
        checksum = decoded[32:34]
        version = decoded[34:]
        print(f"onion public_key:\t {public_key.hex()}")
        print(f"onion checksum:\t {checksum.hex()}")
        print(f"onion version:\t {version.hex()}")

        if (
            checksum
            != hashlib.sha3_256(b".onion checksum" + public_key + version).digest()[:2]
        ):
            raise ValueError
        return public_key, checksum, version
    except:
        raise ValueError("Invalid v3 onion address")


def get_signing_and_public_key_from_hs_ed25519_secret_key(file_content: bytes) -> bytes:
    file_signature = file_content[0:32]
    assert file_signature == b"== ed25519v1-secret: type0 ==\x00\x00\x00"
    file_key_bytes = file_content[32:]
    signing_key = file_key_bytes[:32]
    public_key = file_key_bytes[32:]
    return signing_key, public_key


def create_hs_ed25519_secret_key_content(signing_key: bytes) -> bytes:
    return b"== ed25519v1-secret: type0 ==\x00\x00\x00" + expand_private_key(
        signing_key
    )


def create_hs_ed25519_public_key_content(public_key: bytes) -> bytes:
    assert len(public_key) == 32
    return b"== ed25519v1-public: type0 ==\x00\x00\x00" + public_key


def create_hidden_service_files(private_key: bytes, hidden_service_dir: str) -> None:
    import pwd
    import grp
    from pathlib import Path

    path = Path(hidden_service_dir)
    parent = path.parent.absolute()
    tor_user = parent.owner()
    tor_group = parent.group()
    uid = pwd.getpwnam(tor_user).pw_uid
    gid = grp.getgrnam(tor_group).gr_gid
    if not path.exists():
        # shutil.rmtree(hidden_service_dir)
        os.mkdir(hidden_service_dir)
        os.chmod(hidden_service_dir, 0o700)
        os.chown(hidden_service_dir, uid, gid)

    (
        signing_key,
        verify_key,
    ) = get_signing_and_public_key_from_seed(private_key)
    file_content_secret = create_hs_ed25519_secret_key_content(signing_key)

    secret_file_path = f"{hidden_service_dir}/hs_ed25519_secret_key"
    utils.store_bytes_to_file(file_content_secret, secret_file_path)
    os.chown(secret_file_path, uid, gid)

    public_file_path = f"{hidden_service_dir}/hs_ed25519_public_key"
    file_content_public = create_hs_ed25519_public_key_content(verify_key)
    utils.store_bytes_to_file(file_content_public, public_file_path)
    os.chown(public_file_path, uid, gid)

    hostname_file_path = f"{hidden_service_dir}/hostname"
    onion_address = onion_address_from_public_key(verify_key)
    print(f"onion_address:\t {onion_address}")
    utils.store_string_to_file(onion_address, hostname_file_path)
    os.chown(hostname_file_path, uid, gid)


def check_if_onion_server_is_running(onion_address: str) -> bool:
    onion_address_http = f"http://{onion_address}/"
    rt = RequestsTor(tor_ports=(9050,), tor_cport=9051)
    try:
        data = rt.get(onion_address_http)
        print(
            f"check_if_onion_server_is_running {onion_address_http} data:", data.json()
        )
        return True
    except Exception as e:
        return False
