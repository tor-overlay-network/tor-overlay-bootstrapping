import argparse
import datetime
import os
import threading
from stem import Signal
from stem.control import Controller


from marshmallow import Schema, fields, ValidationError
from flask import Flask, jsonify, request
import sqlite3
import json


import src.bootstrapping.onion.onion_utils as onion_utils
import src.bootstrapping.utils as utils
import src.bootstrapping.crypto as crypto
import src.routing.connection as con
import src.routing.utils as connection_utils

app = Flask(__name__)

FLASK_PORT = 5000


class ConnectSchema(Schema):
    public_key = fields.String(required=True)
    id = fields.Integer(required=True)
    DH = fields.String(required=True)
    sign = fields.String(required=True)


def get_db_filepath(db_name: str):
    if not os.path.exists("db"):
        os.makedirs("db")
    return f"db/{db_name}.db"


def create_db(name):
    c = sqlite3.connect(get_db_filepath(name)).cursor()
    c.execute(
        "CREATE TABLE IF NOT EXISTS PARTICIPANTS("
        "id TEXT, public_key TEXT NOT NULL UNIQUE, created_at TIMESTAMP, valid_until TIMESTAMP)"
    )
    c.connection.close()


def create_connect_response(
    public_key: str,
    id: int,
    public_DH_key: bytes,
    signer: bytes,
    signing_key: bytes,
):
    response = {
        "public_key": public_key,
        "id": id,
        "signer": signer.hex(),
        "DH": public_DH_key.hex(),
    }
    response = crypto.add_signature_to_data(signing_key, signer, response)
    return response


def create_connect_request(
    signing_key: bytes,
    verify_key: bytes,
    public_DH_key: bytes,
    id: int,
):
    response = {
        "public_key": verify_key.hex(),
        "id": id,
        "DH": public_DH_key.hex(),
    }
    response = crypto.add_signature_to_data(signing_key, verify_key, response)
    return response


@app.route("/", methods=["GET"])
def index():
    uptime = datetime.datetime.utcnow() - app.config["started_at_utc"]
    private_keys: crypto.ED25519KeyHandler = app.config.get("private_keys")
    connectionHandler: con.ConnectionHandler = app.config.get("connection_handler")
    response = {
        "private": True,
        "network": "FIXME",
        "uptime": str(uptime),
        "id": connectionHandler.id,
        "started_at_utc": app.config.get("started_at_utc"),
        "signer": private_keys.verify_key_hex,
    }
    return crypto.add_signature_to_data(
        private_keys.signing_key_bytes, private_keys.verify_key_bytes, response
    )


@app.route("/connect", methods=["POST"])
def connect():
    content = request.get_json(silent=False, force=True)
    schema = ConnectSchema()
    try:
        result = utils.validate_response(schema, content)
    except ValidationError as err:
        return jsonify(err.messages), 422

    connectionHandler: con.ConnectionHandler = app.config.get("connection_handler")

    if not connectionHandler.can_add_new_connection:
        return jsonify({"error": "max_connections"}), 400
    private_DH, public_DH = crypto.generate_new_raw_key_pair("x25519")
    retries = 0
    while not utils.update_torpy_consensus_if_necessary() and retries > 3:
        retries += 1
        print("Trying again to update consensus")
    connection_utils.create_connection(
        connectionHandler=connectionHandler,
        my_private_DH=private_DH.hex(),
        peer_public_DH=result.get("DH"),
        peer_id=result.get("id"),
        peer_verify_key=result.get("public_key"),
        is_server=True,
    )
    private_keys: crypto.ED25519KeyHandler = app.config.get("private_keys")
    response = create_connect_response(
        public_key=result.get("public_key"),
        id=int(connectionHandler.id),
        public_DH_key=public_DH,
        signer=private_keys.verify_key_bytes,
        signing_key=private_keys.signing_key_bytes,
    )
    return json.dumps(response, default=str), 200


def status_callback(*args, **kwargs):
    controller = args[0]
    status = args[1]
    print(f"Controller received status: {status}")


def start_private_onion(
    my_private_key: bytes, connectionHandler: con.ConnectionHandler, e: threading.Event
) -> None:
    private_keys = crypto.ED25519KeyHandler(my_private_key)

    onion_address = onion_utils.onion_address_from_public_key(
        private_keys.verify_key_bytes
    )

    with Controller.from_port() as controller:
        controller.authenticate()
        controller.signal(Signal.NEWNYM)
        controller.add_status_listener(status_callback, spawn=True)

        hidden_service_dir = os.path.join(
            controller.get_conf("DataDirectory", "/tmp"),
            onion_address.replace(".onion", ""),
        )
        onion_utils.create_hidden_service_files(my_private_key, hidden_service_dir)

        result = controller.create_hidden_service(
            hidden_service_dir, 80, target_port=FLASK_PORT
        )

        # The hostname is only available when we can read the hidden service
        # directory. This requires us to be running with the same user as tor.
        if result.hostname:
            print(
                f" * Our service is available at {result.hostname}, press ctrl+c to quit"
            )
        else:
            print(
                " * Unable to determine our service's hostname, probably due to being unable to read the hidden service directory"
            )

        try:
            app.config["onion_address"] = onion_address
            app.config["private_keys"] = private_keys

            """  app.config["db_name"] = keyHandler.verify_key_hex
            create_db(app.config["db_name"]) """

            app.config["hidden_service_dir"] = hidden_service_dir
            app.config["connection_handler"] = connectionHandler
            app.config["started_at_utc"] = datetime.datetime.utcnow()

            flask_thread = threading.Thread(
                name="flask_group",
                target=lambda: app.run(port=FLASK_PORT, debug=True, use_reloader=False),
            )
            flask_thread.setDaemon(True)
            flask_thread.start()
            print("private onion up at ", datetime.datetime.now())
            while not e.isSet():
                e.wait(1)
        except KeyboardInterrupt:
            pass
        finally:
            print(" * Shutting down our hidden service")
            controller.remove_hidden_service(hidden_service_dir, 80)


def start_private_onion_threaded(
    my_private_key: bytes, connectionHandler: con.ConnectionHandler, e: threading.Event
) -> threading.Thread:
    thread = threading.Thread(
        name="private_onion",
        target=start_private_onion,
        args=(my_private_key, connectionHandler, e),
    )
    thread.setDaemon(True)
    thread.start()
    return thread


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--id", help="internal id", type=int)
    return parser.parse_args()


if __name__ == "__main__":
    args = _parse_args()
    id = args.id if args.id else 0
    MY_PRIVATE_KEY = crypto.get_or_create_private_key_file(id, "ed25519")
    MY_PUBLIC_KEY = crypto.get_public_key(MY_PRIVATE_KEY, "ed25519")
    print("main private:\t", MY_PRIVATE_KEY.hex())
    print("main public:\t", MY_PUBLIC_KEY.hex())
    connectionHandler = con.ConnectionHandler(id=id)
    e = threading.Event()
    start_private_onion(MY_PRIVATE_KEY, connectionHandler, e)

    """ thread = start_private_onion_threaded(MY_PRIVATE_KEY, connectionHandler, e)
    while thread.is_alive():
        try:
            time.sleep(10)
        except KeyboardInterrupt:
            e.set() """
