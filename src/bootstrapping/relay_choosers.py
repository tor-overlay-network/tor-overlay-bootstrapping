from __future__ import annotations
import random
from typing import TypedDict, Tuple
import src.bootstrapping.utils as utils

import src.bootstrapping.crypto as crypto

import logging

logger = logging.getLogger(__name__)
logger.setLevel(level=logging.INFO)
logging.getLogger("torpy").setLevel(logging.CRITICAL)


Relay = TypedDict("Relay", {"name": str, "address": str, "port": int})


def choose_relay_from_tunnel(
    tun_name: str, namespace="default", time_frame: str = "1 min"
) -> Tuple[str, bytes]:
    logger.info("Choosing relay from tunnel")

    all_relays = utils.get_all_relays()
    num_of_relays = len(all_relays)
    logger.info(f"Number of relays: {num_of_relays}")

    time_stamp = utils.getRotatedTimeStamp(time_frame)
    h = utils.generate_hash_from(time_stamp, namespace, tun_name)

    selected_relay_index = utils.get_relay_index_from_hash(h, num_of_relays)
    selected_relay: Relay = all_relays[selected_relay_index]
    cookie = utils.get_cookie_from_hash(h)

    logger.info(
        f"I want to connect to {selected_relay['name']} at {selected_relay['address']}:{selected_relay['port']} with cookie {cookie}"
    )

    return selected_relay["name"], cookie


def choose_relay_from_secret(
    secret: bytes, time_frame: str = "1 min"
) -> Tuple[str, bytes]:
    logger.info("Choosing relay from secret")

    all_relays = utils.get_all_relays()
    num_of_relays = len(all_relays)
    logger.info(f"Number of relays: {num_of_relays}")

    # time_stamp = utils.getRotatedTimeStamp(time_frame)
    h = utils.generate_hash_from(secret)

    selected_relay_index = utils.get_relay_index_from_hash(h, num_of_relays)
    selected_relay: Relay = all_relays[selected_relay_index]
    cookie = utils.get_cookie_from_hash(h)

    logger.info(
        f"I want to connect to {selected_relay['name']} at {selected_relay['address']}:{selected_relay['port']} with cookie {cookie}"
    )

    return selected_relay["name"], cookie


def choose_relay_from_keys(
    my_private_key: bytes, peer_public_key: bytes, time_frame: str = "1 min"
) -> Tuple[str, bytes]:
    logger.info("Choosing relay from keys")

    all_relays = utils.get_all_relays()
    num_of_relays = len(all_relays)
    logger.info(f"Number of relays: {num_of_relays}")

    logger.info(
        f"\n\nmy_private_key: {my_private_key.hex()} length:{len(my_private_key)}\npeer_public_key: {peer_public_key.hex()} length:{len(peer_public_key)}\n\n"
    )

    derived_secret = crypto.get_derived_secret_from_keys(
        my_private_key, peer_public_key
    )
    logger.info(f"dervied_secret: {derived_secret.hex()}\n\n")

    time_stamp = utils.getRotatedTimeStamp(time_frame)
    # h = utils.generate_hash_from(time_stamp, derived_secret)
    h = utils.generate_hash_from(derived_secret)

    selected_relay_index = utils.get_relay_index_from_hash(h, num_of_relays)
    selected_relay: Relay = all_relays[selected_relay_index]
    cookie = utils.get_cookie_from_hash(h)

    logger.info(
        f"I want to connect to {selected_relay['name']} at {selected_relay['address']}:{selected_relay['port']} with cookie {cookie}"
    )

    return selected_relay["name"], cookie


def get_random_guard_node():
    all_relays = utils.get_all_relays()
    num_of_relays = len(all_relays)
    logger.info(f"Number of relays: {num_of_relays}")

    selected_relay_index = random.randint(0, num_of_relays - 1)
    selected_relay: Relay = all_relays[selected_relay_index]

    logger.info(
        f"Choose random guard node: {selected_relay['name']} at {selected_relay['address']}:{selected_relay['port']}"
    )

    return selected_relay["name"]
