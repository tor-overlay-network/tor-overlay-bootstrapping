import datetime
import logging
from stmpy import Machine


logging.getLogger("stmpy").setLevel(logging.CRITICAL)

DEFAULT_LOG_STATE_ENTRIES = True


def add_state_print_function_to_entry(states):
    for state in states:
        if "entry" in state:
            state["entry"] = f"_log_state_entry('{state['name']}'); {state['entry']}"
        else:
            state["entry"] = f"_log_state_entry('{state['name']}');"


class MachineBase:
    def __init__(
        self, bootstrapper, log_state_entries: bool = DEFAULT_LOG_STATE_ENTRIES
    ):
        self._logger = logging.getLogger(f"Machine {self.name}")
        self._logger.setLevel(logging.DEBUG)
        self.bootstrapper = bootstrapper
        self.machine = None
        self.log_state_entries = log_state_entries
        if self.log_state_entries:
            add_state_print_function_to_entry(self.states)
        self.machine = Machine(
            name=self.name, transitions=self.transitions, states=self.states, obj=self
        )
        self._logger.info(f"Machine {self.name} created")

    def get_states(self):
        return self.states

    def get_transitions(self):
        return self.transitions

    def get_name(self):
        return self.name

    def get_machine(self):
        return self.machine

    def send_to(self, trigger, machine):
        self.bootstrapper.send_to(trigger, machine)

    def _log_state_entry(self, state):
        now = datetime.datetime.now()
        delta = now - self.bootstrapper.started_at
        microsec_since_start = delta.total_seconds()
        print(
            f"---Machine {self.name} entered ### {state} ### at: ### {now}## microsec_since_start: ##{microsec_since_start}##---"
        )

    def end_stm(self):
        self._logger.info(f"end_stm")
        self.machine.terminate()
