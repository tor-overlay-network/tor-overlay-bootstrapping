from src.bootstrapping.crypto import (
    generate_new_raw_key_pair,
    generate_new_raw_key_pair2,
    get_shared_secret,
    get_public_key,
    derive_new_key_from_secret,
    get_derived_secret_from_keys,
)

from nacl.signing import SigningKey


def test_get_public_key():
    """Test that the public key can be derived from the private key"""
    private_key, public_key = generate_new_raw_key_pair()
    assert get_public_key(private_key) == public_key


def test_get_shared_secret():
    """Test that the derived shared secret is the same for both parties"""
    private_key1, public_key1 = generate_new_raw_key_pair()
    private_key2, public_key2 = generate_new_raw_key_pair()
    assert private_key1 != private_key2
    assert public_key1 != public_key2

    shared_secret1 = get_shared_secret(
        private_key1,
        public_key2,
    )

    shared_secret2 = get_shared_secret(
        private_key2,
        public_key1,
    )

    assert shared_secret1 == shared_secret2


def test_derive_new_key_from_secret():
    new_key = derive_new_key_from_secret(b"test")
    assert len(new_key) == 256
    assert len(new_key.hex()) == 512


def test_get_derived_secret_from_keys():
    private_key1 = bytes.fromhex(
        "d0cb8eb8634c56c7a8d964da0b352f8a282ab6c3839cb6f1b6027299cb74a257"
    )
    public_key1 = bytes.fromhex(
        "fe86c4ab15a42586b41a07e890d61476c24395859244593ebe0de9362d8f1f09"
    )

    private_key2 = bytes.fromhex(
        "c88258c175fc5b08fcd45076686d439a04bb7e669b9986db8d006174ea7c575b"
    )
    public_key2 = bytes.fromhex(
        "9661bb1fe129e1e11a74ca82cea8e3fda2e72c4d24a6475b9a87f26643764366"
    )

    new_key1 = get_derived_secret_from_keys(private_key1, public_key2)
    new_key2 = get_derived_secret_from_keys(private_key2, public_key1)

    assert len(new_key1) == 256 == len(new_key2)

    assert new_key1 == new_key2


def test_generate_new_raw_key_pair_ed25519():
    _, private_key, public_key = generate_new_raw_key_pair2(type="ed25519")
    assert len(private_key) == 32
    assert len(public_key) == 32


def test_if_same_seed_always_give_same_signing_key():
    seed = b"12345678901234567890123456789012"
    assert len(seed) == 32
    signing_key1 = SigningKey(seed)
    signing_key2 = SigningKey(seed)
    assert signing_key1.verify_key == signing_key2.verify_key


def test_if_nacl_and_crypto_give_same_public_key():
    seed, private_key, public_key = generate_new_raw_key_pair2(type="ed25519")
    print(f"seed: \t{seed.hex()}\t length: {len(seed)} {type(seed)}")
    print(
        f"ed25519 private:\t{private_key.hex()}\t length: {len(private_key)} {type(private_key)}"
    )
    print(
        f"ed25519 public:\t{private_key.hex()}\t length: {len(private_key)} {type(private_key)}"
    )
    assert len(private_key) == 32
    assert private_key[:32] == public_key
    crypto_public_key = get_public_key(private_key, type="ed25519")
    assert public_key == crypto_public_key
