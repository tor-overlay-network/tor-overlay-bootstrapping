from __future__ import annotations
import datetime
import os
from threading import Lock, Timer
import time
from typing import TypedDict

import src.bootstrapping.crypto as crypto
from marshmallow import ValidationError
from torpy.consesus import TorConsensus
from torpy.documents.network_status import NetworkStatusDocument
from torpy.cache_storage import TorCacheDirStorage

import pandas as pd
import hashlib

Relay = TypedDict("Relay", {"name": str, "address": str, "port": int})

import logging

logger = logging.getLogger(__name__)
logger.setLevel(level=logging.INFO)


def getRotatedTimeStamp(time_frame="1 min"):
    # How long to use 1 relay as RP. Rotates every :30 seconds with the round-function in Pandas when timeframe="1 min".
    return pd.Timestamp.now().round(time_frame).value


def generate_hash_from(*args):
    builder = b""
    for arg in args:
        if type(arg) == bytes:
            builder += arg
        else:
            builder += str(arg).encode()
    return hashlib.sha256(builder)


def get_relay_index_from_hash(hash: hashlib._Hash, num_of_relays: int) -> int:
    n = int(hash.hexdigest(), base=16)  # Convert to integer to be able to use modulo
    return n % num_of_relays


def get_cookie_from_hash(hash: hashlib._Hash) -> bytes:
    return hash.hexdigest().encode()[0:20]


def get_all_relays() -> list[Relay]:
    """
    Reads all relays from file and returns them as a list of lists.
    Each relay is a list that contains [name, address, port].
    """

    all_relays = []
    a_r = []
    blacklist = []
    actual_relays = []
    logger.info(f"Reads relays from file: /root/.local/share/torpy/network_status")
    with open("/root/.local/share/torpy/network_status", "r") as f:
        line = True
        while line:
            line = f.readline()
            data = line.split(" ")
            data_type = data[0]
            if data_type == "r":  # relay
                name = data[1]
                if name in a_r:
                    blacklist.append(name)
                a_r.append(name)
                [address, port] = data[6:8]
                all_relays.append({"name": name, "address": address, "port": port})

    for rel in all_relays:
        if not rel["name"] in blacklist:
            actual_relays.append(rel)
    logger.info(
        f"Done reading relays from file: /root/.local/share/torpy/network_status\n"
    )
    logger.info(f"Found {len(all_relays)} relays")
    logger.info(f"Blacklisted {len(blacklist)} relays")
    logger.info(f"Actual relays {len(actual_relays)}")

    return actual_relays


def generate_openVPN_static_key_file(static_key: bytes, key_name: str):
    """Generate a static openVPN key file
    Output is a file with the following format:
    #
    # 2048 bit OpenVPN static key
    #
    -----BEGIN OpenVPN Static key V1-----
    8e53dda65634fb549fdac0691c197fbb
    705a6d7cccd1016504f83c4110afeb6d
    ca88efe5e01b6be2c69e9811a16ce6e9
    e7e5365a021989005d617ed83f3f2d49
    bca99ae250e0c1883698c02de276777d
    3d3a2f7256e2fea008304fdf23908e47
    08e4b38505cfdb9cb3e0f877f16cb5e6
    a2fabb1bd04ad63847c2c390069a3093
    80af132bebc6b98d20d071476b19693e
    c81a331378171ca75c0962b352391c74
    09795e533984977d204cd4483d6c5c64
    dcd174377910ab72f31a88f9bfd4a2ba
    5c4d0b7548a2dfd5a1e1cd303d2e3394
    c10e75ef1130e5bf6c7cd7f362cf24b5
    b164126e5d175fa03a5a9ebcc0c225a7
    f54bea08b278ef1856716b2db1f3f10d
    -----END OpenVPN Static key V1-----"""
    if len(static_key) != 256:
        raise ValueError("Static key must be 256 bytes long")

    # Convert to hex
    hex_key = static_key.hex()

    # Split into 32 byte chunks
    hex_chunks = "\n".join([hex_key[i : i + 32] for i in range(0, len(hex_key), 32)])

    openvpn_static_key = f"""#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
{hex_chunks}
-----END OpenVPN Static key V1-----"""
    file_path = f"keys/{key_name}.key"
    with open(file_path, "w") as f:
        f.write(openvpn_static_key)

    return file_path


def store_bytes_to_file(bytes: bytes, filename: str):
    with open(filename, "wb") as binary_file:
        binary_file.write(bytes)
    return filename


def store_string_to_file(string: str, filename: str):
    with open(filename, "w") as file:
        file.write(string)
    return filename


def read_bytes_from_file(filename: str):
    with open(filename, mode="rb") as binary_file:
        return binary_file.read()


def validate_response(schema, data):
    try:
        validated_data = schema.load(data)
        if "sign" in validated_data:
            sign_hex = validated_data.pop("sign")
            public_key = validated_data["public_key"]
            h = crypto.get_hash_from_data(validated_data)
            is_verified = crypto.verify_signature(
                bytes.fromhex(public_key), bytes.fromhex(sign_hex), h
            )
            if not is_verified:
                raise ValidationError({"sign": "sign is not valid"})
        return validated_data
    except ValidationError as err:
        raise ValidationError(err.messages)


def utc2local(utc):
    epoch = time.mktime(utc.timetuple())
    offset = datetime.datetime.fromtimestamp(
        epoch
    ) - datetime.datetime.utcfromtimestamp(epoch)
    return utc + offset


def update_torpy_consensus_if_necessary():
    document = TorCacheDirStorage().load_document(NetworkStatusDocument)
    if (
        document != None
        and document.valid_after <= datetime.datetime.utcnow() <= document.fresh_until
    ):
        print(
            "Concensus is up to date and fresh until localtime: ",
            utc2local(document.fresh_until),
            " localtime: ",
            datetime.datetime.now(),
        )
        return True
    if os.path.exists("/root/.local/share/torpy/network_status"):
        os.remove("/root/.local/share/torpy/network_status")
    import requests

    # Unlike the above example, this one downloads specifically through the
    # ORPort of moria1 (long time tor directory authority).
    try:
        url = "http://45.66.33.45/tor/status-vote/current/consensus"  # might be wrong consensus, but can speed things up when it works
        response = requests.get(url)
        print(response.status_code)
        store_string_to_file(response.text, "/root/.local/share/torpy/network_status")
        return True
    except Exception:
        print("Failed to download consensus from url: ", url)
    consensus_client = TorConsensus()  # This will hang around but not block
    document = consensus_client.get_document()
    print(
        "Concensus is up to date and fresh until localtime: ",
        utc2local(document.fresh_until),
        datetime.datetime.now(),
    )
    return True


class Periodic(object):
    """
    A periodic task running in threading.Timers
    """

    def __init__(self, interval, function, *args, **kwargs):
        self._lock = Lock()
        self._timer = None
        self.function = function
        self.interval = interval
        self.args = args
        self.kwargs = kwargs
        self._stopped = True
        if kwargs.pop("autostart", False):
            self.start()

    def start(self, from_run=False):
        self._lock.acquire()
        if from_run or self._stopped:
            self._stopped = False
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
        self._lock.release()

    def _run(self):
        self.start(from_run=True)
        self.function(*self.args, **self.kwargs)

    def stop(self):
        self._lock.acquire()
        if self._timer is not None:
            self._timer.cancel()
        self._stopped = True
        self._lock.release()

    def is_running(self):
        return not self._stopped


if __name__ == "__main__":
    print("starting utils main")
    update_torpy_consensus_if_necessary()
    print("ending utils main")
    os._exit(9)
