import re
import subprocess


def get_tunneled_ip_addresses():
    try:
        output = subprocess.check_output(
            "netstat -rn | grep tun", shell=True, text=True
        )
    except subprocess.CalledProcessError:
        return []
    pattern = r"^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})"
    ips = []
    for line in output.split("\n"):
        match = re.search(pattern, line)
        if match:
            ips.append(match.group(1))
    return ips


# TODO find another way of doing this
def get_tunneled_internal_ids():
    tunneled_ips = get_tunneled_ip_addresses()
    internal_ids = []
    for ip in tunneled_ips:
        internal_ids.append(int(ip.split(".")[1]))
    return internal_ids


if __name__ == "__main__":
    print(get_tunneled_ip_addresses())
