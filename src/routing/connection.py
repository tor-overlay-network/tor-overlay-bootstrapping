import argparse
import datetime
import signal
import subprocess
import sys
import time
import os


import src.routing.check_active_tunnels as check_active_tunnels
import src.bootstrapping.utils as utils
import src.bootstrapping.crypto as crypto

import logging

logger = logging.getLogger(__name__)
logger.setLevel(level=logging.INFO)


class ConnectionSubprocess:
    def __init__(
        self,
        my_private_DH: str,
        my_id: int,
        peer_public_DH: str,
        peer_id: int,
        peer_verify_key: str,
        is_server: bool,
        log_output: bool = False,
        polling_interval: int = 4,
        on_connection_confirmed_callback: callable = None,
        time_guard: int = 0,
    ):
        self.my_private_DH = my_private_DH
        self.peer_public_DH = peer_public_DH
        self.is_server = is_server
        self.my_id = my_id
        self.peer_id = peer_id
        if type(peer_verify_key) is not str:
            raise Exception("peer_verify_key must be an hex string")
        self.peer_verify_key = peer_verify_key
        self.scheduler = utils.Periodic(polling_interval, self.ping)
        self.process = None
        self.started_at = None
        self.last_successfull_ping = None
        self.connection_up = False
        self.on_connection_confirmed_callback: callable = (
            on_connection_confirmed_callback
        )
        self.stdout = subprocess.DEVNULL
        if log_output:
            self.stdout = sys.stdout
        self.time_guard = time_guard

    def start(self):
        if self.is_server:
            self._start_server()
        else:
            self._start_client()
        self.scheduler.start()
        return self

    def is_process_running(self):
        if self.process is None:
            print("process has not been started yet")
            return False
        return self.process.poll() is None

    def ping(self):
        hostname = f"10.{self.peer_id}.0.1"
        process = subprocess.run(
            args=["ping", "-c", "1", "-W", "2", f"{hostname}"],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )
        got_response = (
            process.returncode == 0
        )  # True means the host is up, False means it is down
        # print(f"pinged {self.peer_id} got response: {got_response}")
        now = datetime.datetime.now()
        if got_response:
            try:
                if self.last_successfull_ping == None:
                    self.on_connection_confirmed_callback(now, self.my_id, self.peer_id)
            except Exception as e:
                print("on_connection_confirmed_callback failed: ", e)
            self.last_successfull_ping = now
            self.connection_up = True
        else:
            self.connection_up = False
        return got_response

    def is_responding(self):
        return self.connection_up

    def has_started(self):
        return self.started_at != None

    def has_responded(self):
        return self.last_successfull_ping != None

    def set_on_connection_confirmed_callback(self, on_connection_confirmed_callback):
        self.on_connection_confirmed_callback = on_connection_confirmed_callback

    def __enter__(self):
        return self.start()

    def _start_server(self):
        print(f"starting server __init__ done")
        self.started_at = datetime.datetime.now()
        self.process = subprocess.Popen(
            [
                "python",
                "src/rtun2.py",
                "-l",
                "-rc",
                "keys",
                "-mdh",
                f"{self.my_private_DH}",
                "-pdh",
                f"{self.peer_public_DH}",
                "-i",
                f"{self.my_id}",
                "-d",
                f"{self.peer_id}",
            ],
            #  stdout=subprocess.DEVNULL,
            #  stderr=subprocess.DEVNULL,
        )

    def _start_client(self):
        time.sleep(self.time_guard)
        self.started_at = datetime.datetime.now()
        self.process = subprocess.Popen(
            [
                "python",
                "src/rtun2.py",
                "-c",
                "-rc",
                "keys",
                "-mdh",
                f"{self.my_private_DH}",
                "-pdh",
                f"{self.peer_public_DH}",
                "-i",
                f"{self.my_id}",
                "-d",
                f"{self.peer_id}",
            ],
            # stdout=subprocess.DEVNULL,
            # stderr=subprocess.DEVNULL,
        )

    def get_running_time(self):
        if self.started_at is None:
            return datetime.timedelta(0)
        running_time = datetime.datetime.now() - self.started_at
        return running_time

    def stop(self):
        if self.process is not None:
            self.process.terminate()
        self.scheduler.stop()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()

    def __str__(self) -> str:
        return f"{'IS_SERVER ' if self.is_server else ''}Connection from {self.my_id} to {self.destination_id}"


class ConnectionHandler:
    def __init__(
        self,
        id=None,
        polling_interval=15,
        max_connections=5,
        preffered_connection_amount=2,
        created_at=datetime.datetime.now(),
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.id = id
        self.connections = {}
        self.peer_id_to_public_keys = {}
        self.created_at = created_at
        self.scheduler = utils.Periodic(polling_interval, self.remove_stale_connections)
        self.max_connections = max_connections
        self.preffered_connection_amount = preffered_connection_amount

    def add_connection(self, peer_id: int, connection: ConnectionSubprocess):
        if peer_id is None:
            raise Exception("peer_id is None")
        if type(peer_id) is not int:
            raise Exception("peer_id must be an int")
        if type(peer_id) is not int:
            raise Exception("peer_id must be an int")
        if peer_id in self.connections:
            ids = check_active_tunnels.get_tunneled_internal_ids()
            if peer_id not in ids:
                self.remove_connection(peer_id)
            else:
                raise Exception(
                    f"Connection to {peer_id} already exists and is running"
                )
        connection.set_on_connection_confirmed_callback(self._on_connection_confirmed)
        self.peer_id_to_public_keys[peer_id] = connection.peer_verify_key
        self.connections[peer_id] = connection
        self.connections[peer_id].start()
        print(f"Added connection to {peer_id}")
        if len(self.connections) == 1:
            self.scheduler.start()
            print(f"Started scheduler with interval {self.scheduler.interval}s")

    def remove_connection(self, peer_id: int):
        if peer_id not in self.connections:
            return
        self.peer_id_to_public_keys.pop(peer_id)
        connection = self.connections.pop(peer_id)
        connection.stop()
        self._kill_openvpn_instance_if_running(peer_id)
        if len(self.connections) == 0:
            self.scheduler.stop()
            print("No more connections, sheduler stopped")

    def get_active_connection(self):
        tunneled_ids = check_active_tunnels.get_tunneled_internal_ids()
        return [x for x in tunneled_ids if x != self.id]

    @property
    def can_add_new_connection(self):
        return len(self.get_active_connection()) < self.max_connections

    @property
    def want_new_connection(self):
        return len(self.get_active_connection()) < self.preffered_connection_amount

    def is_connected_to(self, peer_id: int):
        return int(peer_id) in self.connections

    def is_peer_public_connected(self, peer_public_key: str):
        return peer_public_key in self.peer_id_to_public_keys.values()

    def _kill_openvpn_instance_if_running(self, peer_id: int):
        try:
            output = subprocess.check_output(
                f"ps auxww | grep /usr/sbin/openvpn | grep 10.{peer_id}.0.1",
                shell=True,
                text=True,
            )
        except subprocess.CalledProcessError:
            return  # no openvpn instance running
        inputlines = output.splitlines()
        killed_pids = []
        for inputline in inputlines:
            pid = int(inputline.split()[1])
            if pid not in killed_pids:
                killed_pids.append(pid)
                try:
                    os.kill(pid, signal.SIGTERM)
                except ProcessLookupError:
                    continue

    def remove_all_connections(self):
        for connection in self.connections.values():
            connection.stop()
            self._kill_openvpn_instance_if_running(connection.peer_id)
        self.connections = {}
        if len(self.connections) == 0:
            self._running = False
            self.scheduler.stop()

    def set_id(self, id: int):
        if id == self.id:
            return
        self.id = id
        print("new id set, removing all connections")
        self.remove_all_connections()

    def _on_connection_confirmed(
        self,
        now,
        my_id: int,
        peer_id: int,
    ):
        delta = now - self.created_at
        sec_since_start = delta.total_seconds()
        print(
            f"on_connection_confirmed me {my_id} : peer {peer_id} at {now}, ranfor {sec_since_start}"
        )

    def remove_stale_connections(self):
        actual_connections = self.get_active_connection()
        print(
            "Checking connections, actual_connections = ",
            actual_connections,
            " maybeConnection = ",
            list(self.connections.keys()),
        )
        for connection in self.connections.values():
            stale_runtime_before_kill = 60 if connection.is_server else 120
            if not connection.is_process_running():
                if connection.has_started():
                    print("Checking connections, not running, but started")
                    self.remove_connection(connection.peer_id)
                    return
                if connection.get_running_time() > datetime.timedelta(
                    seconds=stale_runtime_before_kill
                ):
                    print("Checking connections, not running, timeout ")
                    self.remove_connection(connection.peer_id)
                    return
            if connection.is_process_running() and not connection.is_responding():
                if connection.get_running_time() > datetime.timedelta(
                    seconds=stale_runtime_before_kill
                ):
                    print("Checking connections, is running, timeout ")
                    self.remove_connection(connection.peer_id)
                    return


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-l",
        "--listen",
        action="store_true",
        help="create the rendezvous point an wait",
    )
    parser.add_argument(
        "-c",
        "--connect",
        action="store_true",
        help="connect to an already established rendezvous point",
    )

    parser.add_argument(
        "-P",
        "--peer_public_key",
        type=str,
        help="provide the public key of the peer to connect to, only used if -rc is set to keys",
    )

    parser.add_argument(
        "-i",
        "--id",
        help="id of our own client(used for addressing and port allocation)",
        type=int,
        required=True,
    )
    parser.add_argument("-d", "--did", help="destination id", type=int, required=True)

    return parser.parse_args()


def _prompt_for_peer_public_key(prompt: str):
    valid_input = False
    key = ""
    while not valid_input:
        key = input(prompt)
        try:
            if len(key) != 64:
                logger.error("Invalid public key, must be 64 characters long (hex)")
                continue
            serialized_key = bytes.fromhex(key)
            if len(serialized_key) == 32:
                valid_input = True
                break
        except:
            logger.error("Invalid public key, must be 64 characters long (hex)")
            continue

    return key


if __name__ == "__main__":
    args = _parse_args()
    if args.listen and args.connect:
        raise Exception("Can't listen and connect at the same time")
    utils.update_torpy_consensus_if_necessary()

    peer_id = args.did
    is_server = False
    if args.listen:
        is_server = True
    my_private_DH_key, my_public_DH_key = crypto.get_or_create_key_pair_file(
        args.id, type="x25519"
    )
    print(f"Your private DH key(hex) is: {my_private_DH_key.hex()}")
    print(f"Your public DH key(hex) is: {my_public_DH_key.hex()}")
    if not args.peer_public_key:
        peer_public_DH = _prompt_for_peer_public_key(
            "Please provide the public key of the peer (hex): "
        )
    else:
        peer_public_DH = args.peer_public_key
    now = datetime.datetime.now()
    connectionHandler = ConnectionHandler(args.id, created_at=now)
    connection = ConnectionSubprocess(
        my_private_DH=my_private_DH_key.hex(),
        my_id=connectionHandler.id,
        peer_public_DH=peer_public_DH,
        peer_id=peer_id,
        peer_verify_key=os.urandom(32).hex(),
        is_server=is_server,
    )
    connectionHandler.add_connection(peer_id, connection)
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        connectionHandler.remove_all_connections()
        sys.exit(0)
