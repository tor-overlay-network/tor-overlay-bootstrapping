"""Generate direct and via linux ip route tables for each node in the network."""
import argparse
from ast import List


def generate_routetables(my_id: int, all_ids: List[int]):
    """When my_id is 1, peer_ids is [2, 3] the following routetables are generated:
    10.0.2.0/24 via 10.0.0.2 metric 100 # Default for 2 subnet
    10.0.3.0/24 via 10.0.0.3 metric 100 # Default for 3 subnet
    10.0.2.0/24 via 10.0.0.3 metric 1000 # If link to 2 is down
    10.0.3.0/24 via 10.0.0.2 metric 1000 # If link to 3 is down
    """
    routes = []
    peer_ids = list(filter(lambda id: id != my_id, all_ids))
    # Generate direct routetables
    for peer_id in peer_ids:
        routes.append(f"10.0.{peer_id}.0/24 via 10.0.0.{peer_id} metric 100")

    # Generate via routetables
    for peer_id in peer_ids:
        for other_peer_id in peer_ids:
            if peer_id != other_peer_id:
                routes.append(
                    f"10.0.{peer_id}.0/24 via 10.0.0.{other_peer_id} metric 1000"
                )
    return routes


def generate_routing_commands(my_id: int, all_ids: List[int]):
    routes = generate_routetables(my_id, all_ids)
    for route in routes:
        print(f"ip route add {route} &&")


def parse_routeing_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--id", help="Your nodeId", type=str, required=True)
    parser.add_argument(
        "-p",
        "--peers",
        nargs="+",
        help="list of int ids for peers",
        type=int,
        required=True,
    )

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_routeing_args()
    my_id = args.id
    all_ids = args.peers
    generate_routing_commands(my_id, all_ids)
