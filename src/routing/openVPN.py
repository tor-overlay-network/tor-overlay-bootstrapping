import subprocess


def start_openVPN_client(openVPN_key_file: str, my_id: int, destination_id: int):
    print("Starting openvpn client")
    return subprocess.Popen(
        [
            "/usr/sbin/openvpn",
            "--remote",
            "127.0.0.1",
            "--proto",
            "tcp-client",
            "--secret",
            openVPN_key_file,
            "--socks-proxy",
            "127.0.0.1",
            f"105{destination_id}",
            "--ifconfig",
            f"10.{my_id}.0.1",
            f"10.{destination_id}.0.1",
            "--dev",
            f"tun{destination_id}",
            "--port",
            f"119{destination_id}",
        ],
        stdout=subprocess.DEVNULL,  # suppress openvpn output
        # stderr=subprocess.DEVNULL,
    )


def start_openVPN_server(openVPN_key_file: str, my_id: int, destination_id: int):
    print("Starting openvpn server")
    return subprocess.Popen(
        [
            "/usr/sbin/openvpn",
            "--proto",
            "tcp-server",
            "--secret",
            openVPN_key_file,
            "--ifconfig",
            f"10.{my_id}.0.1",
            f"10.{destination_id}.0.1",
            "--dev",
            f"tun{destination_id}",
            "--port",
            f"119{my_id}",
        ],
        stdout=subprocess.DEVNULL,  # suppress openvpn output
        # stderr=subprocess.DEVNULL,
    )
