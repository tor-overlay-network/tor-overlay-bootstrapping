import threading
import src.routing.connection as con


def create_connection(
    connectionHandler: con.ConnectionHandler,
    my_private_DH: str,
    peer_public_DH: str,
    peer_id: int,
    peer_verify_key: str,
    is_server: bool,
):
    connection = con.ConnectionSubprocess(
        my_private_DH=my_private_DH,
        my_id=connectionHandler.id,
        peer_public_DH=peer_public_DH,
        peer_id=peer_id,
        peer_verify_key=peer_verify_key,
        is_server=is_server,
        log_output=False,
        time_guard=5,
    )
    connectionHandler.add_connection(peer_id, connection)
    return connection


def create_connection_threaded(
    connectionHandler: con.ConnectionHandler,
    my_private_DH: str,
    peer_public_DH: str,
    peer_id: int,
    peer_verify_key: str,
    is_server: bool,
) -> threading.Thread:
    thread = threading.Thread(
        target=create_connection,
        args=(
            connectionHandler,
            my_private_DH,
            peer_public_DH,
            peer_id,
            peer_verify_key,
            is_server,
        ),
    )
    thread.setDaemon(True)
    thread.start()
    return thread
