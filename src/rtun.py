#!/usr/bin/env python
import argparse

import src.bootstrapping.relay_choosers as rc
import src.bootstrapping.crypto as crypto
import src.bootstrapping.utils as utils
import src.bootstrapping.ipfs.bootstrapper as bootstrapper

import src.routing.server as server
import src.routing.rendezvous as rendezvous
import src.routing.openVPN as openvpn
import src.routing.connection as con

from time import sleep


import coloredlogs, logging

coloredlogs.install(level="DEBUG")
logger = logging.getLogger(__name__)
logging.getLogger("torpy").setLevel(logging.CRITICAL)
logger.setLevel(level=logging.INFO)


def prompt_for_public_key(prompt: str) -> str:
    valid_input = False
    key = ""
    while not valid_input:
        key = input(prompt)
        try:
            if len(key) != 64:
                logger.error("Invalid public key, must be 64 characters long (hex)")
                continue
            serialized_key = bytes.fromhex(key)
            if len(serialized_key) == 32:
                valid_input = True
                break
        except:
            logger.error("Invalid public key, must be 64 characters long (hex)")
            continue

    return key


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-r", "--relay", help="relay to be used as rendezvous point", type=str
    )
    parser.add_argument(
        "-g", "--guard", help="optional router to be used as guard node", type=str
    )
    parser.add_argument(
        "-l",
        "--listen",
        action="store_true",
        help="create the rendezvous point an wait",
    )
    parser.add_argument(
        "-c",
        "--connect",
        action="store_true",
        help="connect to an already established rendezvous point",
    )
    parser.add_argument("-k", "--cookie", help="a 20 byte rendezvous cookie", type=str)
    parser.add_argument(
        "-rc",
        "--relay-chooser",
        help="select relay automatically using the selected method",
        type=str,
    )

    parser.add_argument(
        "-P",
        "--peer_public_key",
        type=str,
        help="provide the public key of the peer to connect to, only used if -rc is set to keys",
    )

    parser.add_argument(
        "-K",
        "--keys",
        action="store_true",
        help="print your public key",
    )

    parser.add_argument(
        "-t",
        "--tunnel_name",
        help="name of the tunnel, default is the two peers" " name concatenated",
        type=str,
    )
    parser.add_argument(
        "-n",
        "--namespace",
        help="secret key used to differentiate between different nets",
        type=str,
    )
    parser.add_argument(
        "-i",
        "--id",
        help="id of our own client(used for addressing and port allocation)",
        type=int,
    )
    parser.add_argument("-d", "--did", help="destination id", type=int)

    parser.add_argument(
        "-R",
        "--renew",
        action="store_true",
        help="renew Tor directory server list",
    )
    parser.add_argument(
        "-x", "--dummy", action="store_true", help="do not connect, only test"
    )
    parser.add_argument(
        "-b",
        "--bootstrapper",
        help="select bootstrapping method",
        type=str,
    )
    parser.add_argument(
        "-tc",
        "--test_connection",
        help="test_connection",
        action="store_true",
    )
    return parser.parse_args()


def main():
    args = parse_args()
    if args.test_connection:
        logger.info("Starting connection test")
        my_id = "3b309b4af7aa04afca2372578cca0284bfbd77bb4fa0e9306e1a6f1ff41788f6"
        peer_id = "cfa117684055944f0c8077c3a207061fe8a3744b4971e2d6072044d28244805d"
        secret = bytes.fromhex(
            "772de2ee6068b7b4b0b0ddeaca76fc95e15218563fe9efba5cdf6cc5e061316486dfe20acfe59029ad137b89ce833dc3b05a2d85fdc58eff5ad296e7ad043c51183ff915cca51b5ed7a75fd0d4a7d6e9470811f4eb568964b5d62d62248a1039e48331880889e4cd19fb11351041b454782ec64cef9610715d7484e1160c7061e82f4e1c0a359091caac7b53532c31813a82faecbf79cdd4913f9dcde3c5623dff31978057846c0be702615f8e0b1f62e92bc32dc0330b07bd55fc490e6297a8516ad5827d5f4ca49cc89a95a640d2f244b98786094d6c555f8c92165fbe6bd162e08f86d0d29c332cb28ec3b8d53d6b958d451fad0b69acbe28c34085b5effd"
        )
        connection = con.Connection(my_id, peer_id, secret, args.listen)
        try:
            connection.start()
        except:
            connection.stop()
    if args.bootstrapper == "pubsub":
        bootstrapper.Bootstrapper().start()
    else:
        my_private_key = crypto.get_or_create_private_key_file(args.id)
        if args.renew:
            utils.get_all_relays()
        if args.keys:
            my_public_key = crypto.get_public_key(my_private_key)
            logger.warning(f"Your private key(hex) is: {my_private_key.hex()}")
            logger.warning(f"Your public key(hex) is: {my_public_key.hex()}")

        if args.connect and args.listen:
            logger.error(f"Illegal combination of arguments")
            exit()

        if args.namespace:
            namespace = args.namespace
        else:
            namespace = "default"

        if args.relay:
            relay_nick = args.relay

        if args.relay_chooser == "pairwise":
            relay_nick, cookie = rc.choose_relay_from_tunnel(
                args.tunnel_name, namespace=namespace
            )
            logger.info(f"Chosen relay: {relay_nick}, cookie: {cookie}")

            openVPN_key_file = "static.key"
        else:
            if args.peer_public_key:
                peer_public_key = bytes.fromhex(args.peer_public_key)
            else:
                peer_public_key = bytes.fromhex(
                    prompt_for_public_key(
                        "Please provide the public key of the peer (hex): "
                    )
                )
            relay_nick, cookie = rc.choose_relay_from_keys(
                my_private_key=my_private_key, peer_public_key=peer_public_key
            )
            derived_key = crypto.get_derived_secret_from_keys(
                my_private_key, peer_public_key
            )
            openVPN_key_file = utils.generate_openVPN_static_key_file(
                derived_key, f"peer{args.id}-peer{args.did}"
            )

        if args.guard:
            guard_nick = args.guard
        else:
            guard_nick = rc.get_random_guard_node()

        if not args.cookie:
            cookie = "00000000000000000001".encode("UTF-8")
        else:
            if len(args.cookie) != 20:
                logger.error("Cookie is of unacceptable length")
                exit()
            cookie = args.cookie.encode("UTF-8")

        if args.dummy:
            exit()

        if args.listen:
            openvpn_client = openvpn.start_openVPN_client(
                openVPN_key_file, args.id, args.did
            )

            rendezvous.two_hop(
                cookie, relay_nick, guard_nick, int("105" + str(args.did))
            )

            openvpn_client.terminate()

        if args.connect:
            logger.info("Starting openvpn server")
            openvpn_server = openvpn.start_openVPN_server(
                openVPN_key_file, args.id, args.did
            )
            sleep(1)
            try:
                server.list_rend_server(cookie, relay_nick)
            except BaseException:
                logger.error("Error, terminating")
            openvpn_server.terminate()


if __name__ == "__main__":
    main()
