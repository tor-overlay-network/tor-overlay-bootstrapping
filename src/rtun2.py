#!/usr/bin/env python
import argparse
from time import sleep
import datetime

import src.bootstrapping.relay_choosers as rc
import src.bootstrapping.crypto as crypto
import src.bootstrapping.utils as utils

import src.routing.server as server
import src.routing.rendezvous as rendezvous
import src.routing.openVPN as openvpn

import coloredlogs, logging

coloredlogs.install(level="DEBUG")
logger = logging.getLogger(__name__)
logging.getLogger("torpy").setLevel(logging.CRITICAL)
logger.setLevel(level=logging.INFO)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-g", "--guard", help="optional router to be used as guard node", type=str
    )
    parser.add_argument(
        "-l",
        "--listen",
        action="store_true",
        help="create the rendezvous point an wait",
    )
    parser.add_argument(
        "-c",
        "--connect",
        action="store_true",
        help="connect to an already established rendezvous point",
    )

    parser.add_argument(
        "-rc",
        "--relay-chooser",
        help="select relay automatically using the selected method",
        type=str,
    )

    parser.add_argument(
        "-mdh",
        "--myDH",
        help="my DH private key (hex)",
        type=str,
    )

    parser.add_argument(
        "-pdh",
        "--peerDH",
        help="peers DH public key (hex)",
        type=str,
    )

    parser.add_argument(
        "-t",
        "--tunnel_name",
        help="name of the tunnel, default is the two peers" " name concatenated",
        type=str,
    )
    parser.add_argument(
        "-n",
        "--namespace",
        help="secret key used to differentiate between different nets",
        type=str,
    )
    parser.add_argument(
        "-i",
        "--id",
        help="id of our own client(used for addressing and port allocation)",
        type=int,
    )
    parser.add_argument("-d", "--did", help="destination id", type=int)
    return parser.parse_args()


def main():
    args = parse_args()
    started_at = datetime.datetime.now()

    if args.connect and args.listen:
        logger.error(f"Illegal combination of arguments")
        exit()

    if args.namespace:
        namespace = args.namespace
    else:
        namespace = "default"

    if args.relay_chooser == "pairwise":
        relay_nick, cookie = rc.choose_relay_from_tunnel(
            args.tunnel_name, namespace=namespace
        )
        logger.info(f"Chosen relay: {relay_nick}, cookie: {cookie}")

        openVPN_key_file = "static.key"
    else:
        if not args.myDH and not args.peerDH:
            logger.error(
                "You need to provide both myDH and peerDH when using DH keys to connect"
            )
            exit()
        my_DH_private_key = bytes.fromhex(args.myDH)
        peer_DH_public_key = bytes.fromhex(args.peerDH)
        relay_nick, cookie = rc.choose_relay_from_keys(
            my_private_key=my_DH_private_key, peer_public_key=peer_DH_public_key
        )
        derived_key = crypto.get_derived_secret_from_keys(
            my_DH_private_key, peer_DH_public_key
        )
        openVPN_key_file = utils.generate_openVPN_static_key_file(
            derived_key, f"peer{args.id}-peer{args.did}"
        )

    if not args.listen and not args.connect:
        logger.error("You need to specify either -l or -c")
        exit()

    if args.listen:
        if args.guard:
            guard_nick = args.guard
        else:
            guard_nick = rc.get_random_guard_node()

        openvpn_client = openvpn.start_openVPN_client(
            openVPN_key_file, args.id, args.did
        )

        rendezvous.two_hop(
            cookie, relay_nick, guard_nick, int("105" + str(args.did)), started_at
        )

        openvpn_client.kill()

    if args.connect:
        openvpn_server = openvpn.start_openVPN_server(
            openVPN_key_file, args.id, args.did
        )
        sleep(1)
        try:
            server.list_rend_server(cookie, relay_nick, started_at)
        except BaseException as e:
            logger.error("Error, terminating", e)
        openvpn_server.terminate()


if __name__ == "__main__":
    main()
